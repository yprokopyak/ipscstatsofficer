﻿using System;

namespace IPSCStatsOfficer.Domain.DTO
{
    public class MatchDto
    {
        public Guid MDId { get; set; }
        public Guid CROId { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime StartAt { get; set; }
        public int WeaponType { get; set; }

        public int MatchScoringType { get; set; }

        public StageDto[] Stages { get; set; }
    }
}
