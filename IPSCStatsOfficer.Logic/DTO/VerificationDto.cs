﻿namespace IPSCStatsOfficer.Domain.DTO
{
    public class VerificationDto
    {
        public CompetitorDto Competitor { get; set; }

        public (StageDto, ScoreDto)[] Scores { get; set; }
    }
}
