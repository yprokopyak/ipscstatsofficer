﻿using System;

namespace IPSCStatsOfficer.Domain.DTO
{
    public class StageDto
    {
        public Guid Id { get; set; }
        public Guid MatchId { get; set; }
        public string Name { get; set; }
        public int ShotCount { get; set; }
        public int PenaltyTargetsCount { get; set; }
        public int AmmoType { get; set; }
        public int MaxPoints { get; set; }

        public Guid SeniorROId { get; set; }
        public Guid? JuniorROId { get; set; }
    }
}
