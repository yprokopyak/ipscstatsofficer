﻿using System;

namespace IPSCStatsOfficer.Domain.DTO
{
    public class RegistrationDto
    {
        public int Category { get; set; }

        public int Class { get; set; }

        public CompetitorDto Competitor { get; set; }

        public Guid CompetitorId { get; set; }

        public Guid MatchId { get; set; }

        public string Name { get; set; }

        public int PowerFactor { get; set; }

        public int Squad { get; set; }
        public string PIN { get; set; }

        public bool HasDQ { get; set; }

        public string DQReason { get; set; }

        public ScoreDto[] Scores { get; set; }
    }
}