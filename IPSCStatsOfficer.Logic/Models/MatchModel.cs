﻿using System;

namespace IPSCStatsOfficer.Domain.Models
{
    public class MatchModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime StartAt { get; set; }
        public int WeaponType { get; set; }

        public int MatchScoringType { get; set; }

        public Guid CROId { get; set; }
        public Guid MDId { get; set; }

        public StageModel[] Stages { get; set; }
    }
}
