﻿using System;

namespace IPSCStatsOfficer.Domain.Models
{
    public class RegistrationModel
    {
        public Guid Id { get; set; }

        public string Category { get; set; }

        public string Class { get; set; }

        public string Name { get; set; }

        public int Squad { get; set; }

        public bool HasDQ { get; set; }

        public string DQReason { get; set; }

        public ScoreModel[] Scores { get; set; }
    }
}