﻿using System.ComponentModel;

namespace IPSCStatsOfficer.Domain.Models
{
    public enum Categories
    {
        [Description("Загальна")]
        Regular = 1,
        [Description("Леді")]
        Lady = 2,
        [Description("Юніор")]
        Junior = 3,
        [Description("Сеньйор")]
        Senior = 4,
        [Description("Суперсеньйор")]
        SuperSenior = 5
    }

    public enum Classes
    {
        [Description("Стандарт-мануал")]
        StandardManual = 1,
        [Description("Стандарт")]
        Standard = 2,
        [Description("Модифікований")]
        Modified = 3,
        [Description("Відкритий")]
        Open = 4,
        [Description("Пістолетний калібр")]
        PCC = 5,
        [Description("Напівавтоматичний-відкритий SAO")]
        SAO = 6,
        [Description("Напівавтоматичний стандартний SAS")]
        SAS = 7
    }

    public enum FirearmType
    {
        [Description("Пістолет")]
        Pistol = 1,
        [Description("Карабін")]
        Carbine = 2,
        [Description("Рушниця")]
        Shotgun = 3
    }

    public enum AmmoType
    {
        [Description("Шріт")]
        BirdShot = 1,
        [Description("Куля")]
        Slug = 2,
        [Description("Картеч")]
        BuckShot = 3
    }

    public enum PowerFactor
    {
        [Description("Мажор")]
        Major = 0,
        [Description("Мінор")]
        Minor = 1
    }

    public enum MatchScoringRules
    {
        [Description("IPSC - Практична стрільба")]
        IPSC = 0,
        [Description("UFPS - Прикладна стрільба")]
        UFPS = 1
    }
}