﻿namespace IPSCStatsOfficer.Domain.Models
{
    public class ImportCompetitorScoreModel
    {
        public int Class { get; set; }

        public string CompetitorName { get; set; }

        public ScoreModel CompetitorScore { get; set; }

        public string StageName { get; set; }
    }
}