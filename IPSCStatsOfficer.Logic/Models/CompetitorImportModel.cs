﻿namespace IPSCStatsOfficer.Domain.Models
{
    public class CompetitorImportModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Squad { get; set; }
        public string Status { get; set; }
        public string Category { get; set; }
        public string Class { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
