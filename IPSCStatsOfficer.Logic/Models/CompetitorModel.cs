﻿using System;

namespace IPSCStatsOfficer.Domain.Models
{
    public class CompetitorModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Category { get; set; }
    }
}
