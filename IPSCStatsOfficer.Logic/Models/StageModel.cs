﻿using System;

namespace IPSCStatsOfficer.Domain.Models
{
    public class StageModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int ShotCount { get; set; }
        public int PenaltyTargetsCount { get; set; }
        public int AmmoType { get; set; }
        public int StageMaxPoints { get; set; }

        public Guid SeniorROId { get; set; }
        public Guid? JuniorROId { get; set; }
    }
}
