﻿namespace IPSCStatsOfficer.Domain.Models.Scores
{
    public class ScoreByCriteriaModel
    {
        public string CriteriaDescription { get; set; }
        public ScoreResultModel[] Results { get; set; }
        public ScoreByCriteriaModel[] SubCriteriaScore { get; set; }
    }
}
