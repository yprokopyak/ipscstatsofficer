﻿using System;

namespace IPSCStatsOfficer.Domain.Models.Scores
{
    public class VerificationModel
    {
        public CompetitorModel Competitor { get; set; }
        
        public (StageModel, ScoreModel)[] Scores { get; set; }
    }
}