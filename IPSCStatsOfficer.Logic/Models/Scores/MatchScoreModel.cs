﻿using System;

namespace IPSCStatsOfficer.Domain.Models.Scores
{
    public class MatchScoreModel
    {
        public MatchScoreModel()
        {
            ByCategory = Array.Empty<ScoreByCriteriaModel>();
            ByClass = Array.Empty<ScoreByCriteriaModel>();
            ByStage = Array.Empty<ScoreByCriteriaModel>();
            ByStageAndCategory = Array.Empty<ScoreByCriteriaModel>();
        }

        public ScoreByCriteriaModel[] ByCategory { get; set; }
        public ScoreByCriteriaModel[] ByClass { get; set; }
        public ScoreByCriteriaModel[] ByStage { get; set; }
        public ScoreByCriteriaModel[] ByStageAndCategory { get; set; }
    }
}