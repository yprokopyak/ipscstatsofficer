﻿using System;

namespace IPSCStatsOfficer.Domain.Models.Scores
{
    public class ScoreResultModel
    {
        public string CompetitorCriteria { get; set; }

        public Guid CompetitorId { get; set; }

        public string CompetitorName { get; set; }

        public decimal HitFactor { get; set; }

        public decimal Percentage { get; set; }

        public decimal Points { get; set; }

        public ScoreModel Score { get; set; }
    }
}