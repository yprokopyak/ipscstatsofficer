﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Extentions;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Domain.Models.Scores;

namespace IPSCStatsOfficer.Domain.BusinessLogic.ScoresCalсulators
{
    public class UFPSScoresCalculator : IScoresCalculator
    {
        private readonly IMapper _mapper;
        public UFPSScoresCalculator(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ScoreByCriteriaModel[] CalculateScoresByClass(
            ScoreByCriteriaModel[] scoresByStage,
            IEnumerable<RegistrationDto> competitorRegistrations)
        {
            var individualScores = scoresByStage.SelectMany(x => x.SubCriteriaScore)
                .SelectMany(x => x.Results)
                .GroupBy(x => x.CompetitorId)
                .Select(
                    x => new ScoreResultModel
                    {
                        CompetitorId = x.Key,
                        Points = x.Sum(y => y.Points),
                        CompetitorCriteria =
                                     ((Classes)competitorRegistrations
                                             .FirstOrDefault(y => y.CompetitorId == x.Key)
                                             .Class).GetEnumDescription(),
                        CompetitorName = $"{x.FirstOrDefault()?.CompetitorName}"
                    });

            var classScores = individualScores.GroupBy(x => x.CompetitorCriteria).ToArray();

            foreach (var classScore in classScores)
            {
                var maxScoreInClass = classScore.Max(x => x.Points);
                foreach (var competitorScore in classScore)
                    competitorScore.Percentage = maxScoreInClass == 0 
                                                     ? 0 
                                                     : Math.Round(
                                                         competitorScore.Points / maxScoreInClass * 100,
                        2,
                        MidpointRounding.AwayFromZero);
            }

            return classScores.Select(
                    x => new ScoreByCriteriaModel
                    {
                        CriteriaDescription = x.Key,
                        Results = x.OrderByDescending(y => y.Percentage).ToArray()
                    })
                .ToArray();
        }

        public ScoreByCriteriaModel[] CalculateScoresByClassAndCategory(
            ScoreByCriteriaModel[] scoresByStageAndCategory,
            IEnumerable<RegistrationDto> competitorRegistrations)
        {
            return new ScoreByCriteriaModel[] { };
        }

        public ScoreByCriteriaModel CalculateScoresByStage(string stageName, ScoreDto[] stageScores)
        {
            var scoresByClass = CalculateStageScoresByClass(stageScores);
            return new ScoreByCriteriaModel
            {
                CriteriaDescription = $"Вправа {stageName}",
                SubCriteriaScore = scoresByClass
            };
        }

        public ScoreByCriteriaModel CalculateScoresByStageAndCategory(string stageName, ScoreDto[] stageScores)
        {
            return new ScoreByCriteriaModel();
        }

        private decimal CalculateHitFactor(ScoreDto score)
        {
            if (score.Time == 0)
                return 0;
            return GetPoints(score);
        }

        private ScoreByCriteriaModel[] CalculateStageScoresByClass(ScoreDto[] stageScores)
        {
            if (!stageScores.Any())
                return new ScoreByCriteriaModel[0];

            var stage = stageScores[0].Stage;
            var stageResult = stageScores.Select(
                    x => new ScoreResultModel
                    {
                        HitFactor = CalculateHitFactor(x),
                        Score = _mapper.Map<ScoreModel>(x),
                        CompetitorName = $"{x.Competitor.LastName} {x.Competitor.FirstName}",
                        CompetitorCriteria =
                                     ((Classes)x.CompetitorRegistration.Class).GetEnumDescription(),
                        CompetitorId = x.Competitor.Id
                    })
                .ToArray();

            //calcualte percentage and points
            foreach (var scoresPerCriteria in stageResult.GroupBy(x => x.CompetitorCriteria))
            {
                var minHitFactor = scoresPerCriteria.Min(x => x.HitFactor);
                foreach (var score in scoresPerCriteria)
                {
                    score.Percentage = minHitFactor == 0 ? 0 : Math.Round(
                                           minHitFactor / score.HitFactor * 100,
                        2,
                        MidpointRounding.AwayFromZero);
                    score.Points = score.Percentage;
                }
            }

            return stageResult.GroupBy(x => x.CompetitorCriteria)
                .Select(x => new ScoreByCriteriaModel { CriteriaDescription = x.Key, Results = x.OrderByDescending(y => y.Percentage).ToArray() })
                .ToArray();
        }

        private decimal GetPoints(ScoreDto score)
        {
            decimal stagePoints = score.Time + score.C * 1 + score.D * 5 + score.Miss * 10 + score.PT * 30
                              - score.EngageError * 30 + score.Proc * 20 + score.SpecialProc * 50 + score.UnpropperExecution * 200;

            return stagePoints >= 0 ? stagePoints : 0;
        }
    }
}
