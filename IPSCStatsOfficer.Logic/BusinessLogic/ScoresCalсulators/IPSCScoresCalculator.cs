﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Extentions;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Domain.Models.Scores;

namespace IPSCStatsOfficer.Domain.BusinessLogic.ScoresCalсulators
{
    public class IPSCScoresCalculator : IScoresCalculator
    {
        private readonly IMapper _mapper;
        public IPSCScoresCalculator(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ScoreByCriteriaModel[] CalculateScoresByClass(
            ScoreByCriteriaModel[] scoresByStage,
            IEnumerable<RegistrationDto> competitorRegistrations)
        {
            var individualScores = scoresByStage.SelectMany(x => x.SubCriteriaScore)
                .SelectMany(x => x.Results)
                .GroupBy(x => x.CompetitorId)
                .Select(
                    x => new ScoreResultModel
                             {
                                 CompetitorId = x.Key,
                                 Points = x.Sum(y => y.Points),
                                 CompetitorCriteria =
                                     ((Classes)competitorRegistrations
                                             .FirstOrDefault(y => y.CompetitorId == x.Key)
                                             .Class).GetEnumDescription(),
                                 CompetitorName = $"{x.FirstOrDefault()?.CompetitorName}"
                             });

            var classScores = individualScores.GroupBy(x => x.CompetitorCriteria).ToArray();

            foreach (var classScore in classScores)
            {
                var maxScoreInClass = classScore.Max(x => x.Points);
                foreach (var competitorScore in classScore)
                    competitorScore.Percentage = maxScoreInClass == 0 ? 0 : Math.Round(
                        competitorScore.Points / maxScoreInClass * 100,
                        4,
                        MidpointRounding.AwayFromZero);
            }

            return classScores.Select(
                    x => new ScoreByCriteriaModel
                             {
                                 CriteriaDescription = x.Key,
                                 Results = x.OrderByDescending(y => y.Percentage).ToArray()
                             })
                .ToArray();
        }

        public ScoreByCriteriaModel[] CalculateScoresByClassAndCategory(
            ScoreByCriteriaModel[] scoresByStageAndCategory,
            IEnumerable<RegistrationDto> competitorRegistrations)
        {
            var categories = competitorRegistrations.Select(x => ((Categories)x.Category).GetEnumDescription())
                .Distinct();

            return categories.Select(
                    category =>
                        {
                            var stageResultByCategory = scoresByStageAndCategory.Select(
                                    x => new ScoreByCriteriaModel
                                             {
                                                 CriteriaDescription = x.CriteriaDescription,
                                                 SubCriteriaScore =
                                                     x.SubCriteriaScore.FirstOrDefault(
                                                             y => y.CriteriaDescription == category)
                                                         .SubCriteriaScore
                                             })
                                .ToArray();
                            return new ScoreByCriteriaModel
                                       {
                                           CriteriaDescription = category,
                                           SubCriteriaScore = CalculateScoresByClass(
                                               stageResultByCategory,
                                               competitorRegistrations)
                                       };
                        })
                .ToArray();
        }

        public ScoreByCriteriaModel CalculateScoresByStage(string stageName, ScoreDto[] stageScores)
        {
            var scoresByClass = CalculateStageScoresByClass(stageScores);
            return new ScoreByCriteriaModel
                       {
                           CriteriaDescription = $"Вправа {stageName}",
                           SubCriteriaScore = scoresByClass
                       };
        }

        public ScoreByCriteriaModel CalculateScoresByStageAndCategory(string stageName, ScoreDto[] stageScores)
        {
            var scoresByCategory = stageScores.GroupBy(x => x.CompetitorRegistration.Category);

            return new ScoreByCriteriaModel
                       {
                           CriteriaDescription = $"Вправа {stageName}",
                           SubCriteriaScore = scoresByCategory.Select(
                                   x => new ScoreByCriteriaModel
                                            {
                                                CriteriaDescription =
                                                    ((Categories)x.Key)
                                                    .GetEnumDescription(),
                                                SubCriteriaScore =
                                                    CalculateStageScoresByClass(
                                                        x.ToArray())
                                            })
                               .ToArray()
                       };
        }

        private decimal CalculateHitFactor(ScoreDto score)
        {
            if (score.Time == 0)
                return 0;
            return Math.Round(GetPoints(score) / score.Time, 4, MidpointRounding.AwayFromZero);
        }

        private ScoreByCriteriaModel[] CalculateStageScoresByClass(ScoreDto[] stageScores)
        {
            if (!stageScores.Any())
                return new ScoreByCriteriaModel[0];

            var stage = stageScores[0].Stage;
            var stageResult = stageScores.Select(
                    x => new ScoreResultModel
                             {
                                 HitFactor = CalculateHitFactor(x),
                                 Score = _mapper.Map<ScoreModel>(x),
                                 CompetitorName = $"{x.Competitor.LastName} {x.Competitor.FirstName}",
                                 CompetitorCriteria =
                                     ((Classes)x.CompetitorRegistration.Class).GetEnumDescription(),
                                 CompetitorId = x.Competitor.Id
                             })
                .ToArray();

            //calcualte percentage and points
            foreach (var scoresPerCriteria in stageResult.GroupBy(x => x.CompetitorCriteria))
            {
                var maxHitFactor = scoresPerCriteria.Max(x => x.HitFactor);
                foreach (var score in scoresPerCriteria)
                {
                    score.Percentage = maxHitFactor == 0 ? 0 : Math.Round(
                        score.HitFactor / maxHitFactor * 100,
                        2,
                        MidpointRounding.AwayFromZero);
                    score.Points = maxHitFactor == 0 ? 0 : Math.Round(
                        stage.MaxPoints * score.HitFactor / maxHitFactor,
                        2,
                        MidpointRounding.AwayFromZero);
                }
            }

            return stageResult.GroupBy(x => x.CompetitorCriteria)
                .Select(x => new ScoreByCriteriaModel { CriteriaDescription = x.Key, Results = x.OrderByDescending(y => y.Percentage).ToArray() })
                .ToArray();
        }

        private decimal GetPoints(ScoreDto score)
        {
            var stagePoints = 0;
            if (score.CompetitorRegistration.PowerFactor == (int)PowerFactor.Major)
            {
                stagePoints = score.A * 5 + score.C * 4 + score.D * 2 - score.Miss * 10 - score.PT * 10
                              - score.Proc * 10;
            }
            else if (score.CompetitorRegistration.PowerFactor == (int)PowerFactor.Minor)
            {
                stagePoints = score.A * 5 + score.C * 3 + score.D * 1 - score.Miss * 10 - score.PT * 10
                              - score.Proc * 10;
            }

            return stagePoints >= 0 ? stagePoints : 0;
        }
    }
}