﻿using System.Collections.Generic;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Models.Scores;

namespace IPSCStatsOfficer.Domain.BusinessLogic.ScoresCalсulators
{
    public interface IScoresCalculator
    {
        ScoreByCriteriaModel CalculateScoresByStage(string stageName, ScoreDto[] stageScores);
        ScoreByCriteriaModel CalculateScoresByStageAndCategory(string stageName, ScoreDto[] stageScores);
        ScoreByCriteriaModel[] CalculateScoresByClassAndCategory(ScoreByCriteriaModel[] scoresByStage, IEnumerable<RegistrationDto> competitorRegistrations);

        ScoreByCriteriaModel[] CalculateScoresByClass(ScoreByCriteriaModel[] scoresByStage, IEnumerable<RegistrationDto> competitorRegistrations);
    }
}
