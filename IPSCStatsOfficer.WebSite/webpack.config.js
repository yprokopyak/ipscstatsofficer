﻿const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');


const env = process.env.NODE_ENV;
const ASSET_PATH = process.env.ASSET_PATH || '/';

module.exports = {
    entry: { main: './ClientApp/boot.ts' },
    mode: "development",
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'wwwroot/dist'),
        publicPath: ASSET_PATH
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                loader: 'vue-loader',
                options: {
                    hotReload: true
                }
            },
            {
                test: /\.(scss)$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ["*",".html",".tsx", ".ts", ".js"]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
};