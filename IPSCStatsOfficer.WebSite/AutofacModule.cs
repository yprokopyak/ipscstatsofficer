﻿using Autofac;
using AutoMapper;
using IPSCStatsOfficer.Domain.BusinessLogic.ScoresCalсulators;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.EntityFramework;
using IPSCStatsOfficer.Repository;
using IPSCStatsOfficer.Services;

namespace IPSCStatsOfficer.WebSite
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var config = new MapperConfiguration(
                cfg =>
                    {
                        cfg.AddProfile<WebsiteAutomapProfile>();
                        cfg.AddProfile<RepositoryProfile>();
                    });

            var mapper = config.CreateMapper();

            builder.Register(_ => mapper).As<IMapper>().SingleInstance();
            builder.Register(c => new IPSCStatsOfficerDataContext())
                .As<IIPSCStatsOfficerDataContext>()
                .InstancePerLifetimeScope();

            //Repositories
            builder.RegisterType<CompetitorsRepository>().As<ICompetitorsRepository>().InstancePerLifetimeScope();
            builder.RegisterType<MatchRepository>().As<IMatchRepository>().InstancePerLifetimeScope();
            builder.RegisterType<MatchRegistrationRepository>()
                .As<IMatchRegistrationRepository>()
                .InstancePerLifetimeScope();
            builder.RegisterType<ScoreRepository>().As<IScoreRepository>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            //Services
            builder.RegisterType<CompetitorsService>().As<ICompetitorsService>().InstancePerLifetimeScope();
            builder.RegisterType<MatchService>().As<IMatchService>().InstancePerLifetimeScope();
            builder.RegisterType<ScoreService>().As<IScoreService>().InstancePerLifetimeScope();
            builder.RegisterType<LoginService>().As<ILoginService>().InstancePerLifetimeScope();
            builder.RegisterType<RegistrationService>().As<IRegistrationService>().InstancePerLifetimeScope();
            builder.RegisterType<ImportScoresService>().As<IImportScoresService>().InstancePerLifetimeScope();

            builder.RegisterType<IPSCScoresCalculator>().Keyed<IScoresCalculator>((int)MatchScoringRules.IPSC);
            builder.RegisterType<UFPSScoresCalculator>().Keyed<IScoresCalculator>((int)MatchScoringRules.UFPS);
        }
    }
}