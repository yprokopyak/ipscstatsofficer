using System;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Extentions;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Domain.Models.Scores;
using IPSCStatsOfficer.WebSite.Models;
using IPSCStatsOfficer.WebSite.Models.Scores;

namespace IPSCStatsOfficer.WebSite
{
    public class WebsiteAutomapProfile : Profile
    {
        public WebsiteAutomapProfile()
        {
            CreateMap<StageApiModel, StageModel>()
                .ForMember(dst => dst.Id, x => x.ResolveUsing(
                    src => string.IsNullOrEmpty(src.Id)
                               ? new Guid()
                               : Guid.Parse(src.Id)))
                .ForMember(
                    dst => dst.AmmoType,
                    x => x.ResolveUsing(src => src.AmmoType.GetValueFromDescription<AmmoType>()))
                .ForMember(dst => dst.SeniorROId, x => x.ResolveUsing(src => Guid.Parse(src.SeniorRO)))
                .ForMember(
                    dst => dst.JuniorROId,
                    x => x.ResolveUsing(
                        src => string.IsNullOrEmpty(src.JuniorRO) ? null : (Guid?)Guid.Parse(src.JuniorRO)));

            CreateMap<StageModel, StageApiModel>()
                .ForMember(
                    dst => dst.AmmoType,
                    x => x.ResolveUsing(src => ((AmmoType)src.AmmoType).GetEnumDescription()))
                .ForMember(dst => dst.SeniorRO, x => x.ResolveUsing(src => src.SeniorROId.ToString()))
                .ForMember(dst => dst.JuniorRO, x => x.ResolveUsing(src => src.JuniorROId.ToString()))
                .ForMember(dst => dst.Id, x => x.ResolveUsing(src => src.Id.ToString()));

            CreateMap<CompetitorImportModel, CompetitorDto>()
                .ForMember(
                    dst => dst.Category,
                    x => x.ResolveUsing(src => src.Category.GetValueFromDescription<Categories>()));

            CreateMap<CompetitorModel, CompetitorApiModel>();
            CreateMap<CompetitorApiModel, CompetitorModel>()
                .ForMember(
                    dst => dst.Category,
                    x => x.ResolveUsing(src => src.Category.GetValueFromDescription<Categories>()));

            CreateMap<MatchApiModel, MatchModel>()
                .ForMember(dst => dst.StartAt, x => x.ResolveUsing(src => DateTime.Parse(src.Date)))
                .ForMember(dst => dst.CROId, x => x.ResolveUsing(src => Guid.Parse(src.CROId)))
                .ForMember(dst => dst.MDId, x => x.ResolveUsing(src => Guid.Parse(src.MDId)))
                .ForMember(
                    dst => dst.WeaponType,
                    x => x.ResolveUsing(src => src.WeaponType.GetValueFromDescription<FirearmType>()))
                .ForMember(
                    dst => dst.MatchScoringType,
                    x => x.ResolveUsing(src => src.MatchScoringType.GetValueFromDescription<MatchScoringRules>()));

            CreateMap<MatchModel, MatchApiModel>()
                .ForMember(dst => dst.Date, x => x.ResolveUsing(src => src.StartAt.ToString("dd/MM/yyy")))
                .ForMember(dst => dst.CROId, x => x.ResolveUsing(src => src.CROId.ToString()))
                .ForMember(dst => dst.MDId, x => x.ResolveUsing(src => src.MDId.ToString()))
                .ForMember(
                    dst => dst.WeaponType,
                    x => x.ResolveUsing(src => ((FirearmType)src.WeaponType).GetEnumDescription()))
                .ForMember(dst => dst.Stages, x => x.ResolveUsing(src => src.Stages.OrderBy(s => s.Name)));

            CreateMap<MatchModel, MatchDto>().ForMember(dst => dst.Id, x => x.Ignore());

            CreateMap<RegistrationModel, RegistrationApiModel>()
                .ForMember(dst => dst.Id, x => x.ResolveUsing(src => src.Id.ToString()));

            CreateMap<ScoreByCriteriaModel, ScoreByCriteriaApiModel>();
            CreateMap<MatchScoreModel, MatchScoreApiModel>();
            CreateMap<ScoreModel, ScoreApiModel>();
            CreateMap<ScoreApiModel, ScoreModel>();
            CreateMap<ImportCompetitorScoreApiModel, ImportCompetitorScoreModel>();

            CreateMap< (StageModel, ScoreModel), (StageApiModel, ScoreApiModel)>()
                .ForMember(dst => dst.Item1, x => x.ResolveUsing(src => src.Item1))
                .ForMember(dst => dst.Item2, x => x.ResolveUsing(src => src.Item2));
            CreateMap<VerificationModel, VerificationApiModel>();

            CreateMap<ScoreResultModel, ScoreResultApiModel>()
                .ForMember(dst => dst.Percentage, x => x.ResolveUsing(src => src.Percentage.ToString("##0.00")))
                .ForMember(dst => dst.HitFactor, x => x.ResolveUsing(src => src.HitFactor.ToString("#0.0000")))
                .ForMember(dst => dst.StagePoints, x => x.ResolveUsing(src => src.Points.ToString("##0.0000")));

            CreateMap<NewRegistrationRequest, RegistrationDto>()
                .ForMember(d => d.Category, x => x.ResolveUsing(s => s.Category.GetValueFromDescription<Categories>()))
                .ForMember(d => d.Class, x => x.ResolveUsing(s => s.Class.GetValueFromDescription<Classes>()))
                .ForMember(d => d.PowerFactor, x => x.ResolveUsing(s => s.PowerFactor.GetValueFromDescription<PowerFactor>()))
                .ForMember(d => d.Squad, x => x.ResolveUsing(s => s.SquadNr))
                .ForMember(d => d.CompetitorId, x => x.ResolveUsing(s => s.CompetitorId))
                .ForMember(d => d.MatchId, x => x.ResolveUsing(s => s.MatchId));
        }
    }
}