﻿using System;

namespace IPSCStatsOfficer.WebSite.Models
{
    public class MatchApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string WeaponType { get; set; }

        public string MatchScoringType { get; set; }

        public string CROId { get; set; }
        public string MDId { get; set; }

        public StageApiModel[] Stages { get; set; }
    }
}
