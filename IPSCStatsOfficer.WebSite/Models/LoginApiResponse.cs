﻿namespace IPSCStatsOfficer.WebSite.Models
{
    public class LoginApiResponse
    {
        public string Roles { get; set; }

        public string Token { get; set; }
    }
}