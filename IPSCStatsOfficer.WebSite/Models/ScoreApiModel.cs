﻿using System;

namespace IPSCStatsOfficer.WebSite.Models
{
    public class ScoreApiModel
    {
        public decimal Time { get; set; }
        public int A { get; set; }
        public int C { get; set; }
        public int D { get; set; }
        public int Miss { get; set; }
        public int PT { get; set; }
        public int Proc { get; set; }
        public int EngageError { get; set; }
        public int SpecialProc { get; set; }
        public int UnpropperExecution { get; set; }
        public Guid StageId { get; set; }

        public Guid CompetitorId { get; set; }

        public Guid MatchId { get; set; }

        public StageApiModel Stage { get; set; }
    }
}
