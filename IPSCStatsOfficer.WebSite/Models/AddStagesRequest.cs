﻿using System;

namespace IPSCStatsOfficer.WebSite.Models
{
    public class AddStagesRequest
    {
        public Guid MatchId { get; set; }

        public StageApiModel Stage { get; set; }
    }
}