﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPSCStatsOfficer.WebSite.Models
{
    public class NewRegistrationRequest
    {
        public Guid MatchId { get; set; }
        public Guid CompetitorId { get; set; }
        public string Class { get; set; }
        public int SquadNr { get; set; }
        public string Category { get; set; }
        public string PowerFactor { get; set; }
    }
}
