﻿namespace IPSCStatsOfficer.WebSite.Models.Scores
{
    public class VerificationApiModel
    {
        public CompetitorApiModel Competitor { get; set; }
        
        public (StageApiModel, ScoreApiModel)[] Scores { get; set; }
    }
}