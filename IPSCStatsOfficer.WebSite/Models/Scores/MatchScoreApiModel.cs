﻿namespace IPSCStatsOfficer.WebSite.Models.Scores
{
    public class MatchScoreApiModel
    {
        public ScoreByCriteriaApiModel[] ByCategory { get; set; }

        public ScoreByCriteriaApiModel[] ByClass { get; set; }

        public ScoreByCriteriaApiModel[] ByStage { get; set; }

        public ScoreByCriteriaApiModel[] ByStageAndCategory { get; set; }
    }
}