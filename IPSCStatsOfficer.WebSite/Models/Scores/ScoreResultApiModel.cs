﻿namespace IPSCStatsOfficer.WebSite.Models.Scores
{
    public class ScoreResultApiModel
    {
        public string CompetitorCriteria { get; set; }

        public string CompetitorName { get; set; }

        public string HitFactor { get; set; }

        public string Percentage { get; set; }

        public string StagePoints { get; set; }

        public ScoreApiModel Score { get; set; }
    }
}