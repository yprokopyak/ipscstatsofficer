﻿namespace IPSCStatsOfficer.WebSite.Models.Scores
{
    public class ScoreByCriteriaApiModel
    {
        public string CriteriaDescription { get; set; }
        public ScoreResultApiModel[] Results { get; set; }
        public ScoreByCriteriaApiModel[] SubCriteriaScore { get; set; }
    }
}
