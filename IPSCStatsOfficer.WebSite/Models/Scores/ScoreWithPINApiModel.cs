﻿namespace IPSCStatsOfficer.WebSite.Models.Scores
{
    public class ScoreWithPINApiModel
    {
        public string PIN { get; set; }

        public ScoreApiModel Score { get; set; }
    }
}