﻿using System;

namespace IPSCStatsOfficer.WebSite.Models.Scores
{
    public class DisqualicationApiModel
    {
        public Guid CompetitorId { get; set; }
        public Guid MatchId { get; set; }
        public string Reason { get; set; }
    }
}
