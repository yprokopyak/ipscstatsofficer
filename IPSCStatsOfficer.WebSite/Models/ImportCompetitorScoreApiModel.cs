﻿namespace IPSCStatsOfficer.WebSite.Models
{
    public class ImportCompetitorScoreApiModel
    {
        public string CompetitorName { get; set; }

        public int Class { get; set; }

        public string StageName { get; set; }

        public ScoreApiModel CompetitorScore { get; set; }
    }
}
