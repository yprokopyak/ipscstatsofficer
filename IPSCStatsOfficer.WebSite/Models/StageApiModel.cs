﻿namespace IPSCStatsOfficer.WebSite.Models
{
    public class StageApiModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public int ShotCount { get; set; }
        public int PenaltyTargetsCount { get; set; }
        public string AmmoType { get; set; }
        public int StageMaxPoints { get; set; }

        public string SeniorRO { get; set; }
        public string JuniorRO { get; set; }
    }
}
