﻿using System;

namespace IPSCStatsOfficer.WebSite.Models
{
    public class CompetitorApiModel
    {
        public Guid? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Category { get; set; }
    }
}
