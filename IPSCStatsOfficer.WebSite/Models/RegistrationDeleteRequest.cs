﻿using System;

namespace IPSCStatsOfficer.WebSite.Models
{
    public class RegistrationDeleteRequest
    {
        public Guid CompetitorId { get; set; }

        public Guid MatchId { get; set; }
    }
}