﻿namespace IPSCStatsOfficer.WebSite.Models
{
    public class ImportCompetitorsRequest
    {
        public string FileContent { get; set; }
        public string SelectedMatchId { get; set; }
    }
}
