﻿namespace IPSCStatsOfficer.WebSite.Models
{
    public class RegistrationApiModel
    {
        public string Id { get; set; }

        public string Category { get; set; }

        public string Class { get; set; }

        public string Name { get; set; }

        public int Squad { get; set; }

        public bool HasDQ { get; set; }

        public string DQReason { get; set; }

        public ScoreApiModel[] Scores { get; set; }
    }
}