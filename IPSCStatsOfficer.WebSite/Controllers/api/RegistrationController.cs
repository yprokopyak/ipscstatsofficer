﻿using System;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Services;
using IPSCStatsOfficer.WebSite.Models;
using Microsoft.AspNetCore.Mvc;

namespace IPSCStatsOfficer.WebSite.Controllers.api
{
    [Route("api/[controller]")]
    public class RegistrationController : Controller
    {
        private readonly IRegistrationService _registrationService;
        private readonly IMapper _mapper;

        public RegistrationController(IRegistrationService registrationService, IMapper mapper)
        {
            _registrationService = registrationService;
            _mapper = mapper;
        }

        [HttpPost("registerCompetitor")]
        public void RegisterCompetitor([FromBody]NewRegistrationRequest request)
        {
            _registrationService.RegisterCompetitor(_mapper.Map<RegistrationDto>(request));
        }

        [HttpGet("formatch")]
        public RegistrationApiModel[] Registrations(Guid matchId)
        {
            var registrations = _registrationService.GetMatchRegistrations(matchId);
            return _mapper.Map<RegistrationApiModel[]>(registrations)
                .OrderBy(x => x.Squad)
                .ThenBy(x => x.Name)
                .ToArray();
        }

        [HttpPost("delete")]
        public void RemoveCompetitorRegistration([FromBody]RegistrationDeleteRequest request)
        {
            _registrationService.RemoveRegistration(request.MatchId, request.CompetitorId);
        }
    }
}