﻿using System;
using AutoMapper;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Domain.Models.Scores;
using IPSCStatsOfficer.Services;
using IPSCStatsOfficer.WebSite.Models;
using IPSCStatsOfficer.WebSite.Models.Scores;
using Microsoft.AspNetCore.Mvc;

namespace IPSCStatsOfficer.WebSite.Controllers.api
{
    [Route("api/[controller]")]
    public class ScoresController
    {
        private readonly IMapper _mapper;
        private readonly IScoreService _scoresService;

        public ScoresController(IScoreService scoresService, IMapper mapper)
        {
            _scoresService = scoresService;
            _mapper = mapper;
        }

        [HttpPost("addcompetitorscore")]
        public void AddCompetitorScore([FromBody]ScoreWithPINApiModel scoreWithPIN)
        {
            _scoresService.AddCompetitorScore(_mapper.Map<ScoreModel>(scoreWithPIN.Score), scoreWithPIN.PIN);
        }

        [HttpPost("dqCompetitor")]
        public void DisqualifyCompetitor([FromBody]DisqualicationApiModel dqModel)
        {
            _scoresService.DisqualifyCompetitor(dqModel.CompetitorId, dqModel.MatchId, dqModel.Reason);
        }

        [HttpGet("scoresForMatch")]
        public MatchScoreApiModel ScoresForMatch(Guid matchId)
        {
            return _mapper.Map<MatchScoreApiModel>(_scoresService.ScoresForMatch(matchId));
        }

        [HttpGet("verificationForMatch")]
        public VerificationApiModel[] VerificationForMatch(Guid matchId)
        {
            return _mapper.Map<VerificationApiModel[]>(_scoresService.VerificationForMatch(matchId));
        }
    }
}