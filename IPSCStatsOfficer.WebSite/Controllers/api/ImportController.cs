﻿using System;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Services;
using IPSCStatsOfficer.WebSite.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace IPSCStatsOfficer.WebSite.Controllers.api
{
    [Route("api/[controller]")]
    public class ImportController : Controller
    {
        private readonly IRegistrationService _registrationService;
        private readonly IImportScoresService _importScoresService;
        private readonly IMapper _mapper;


        public ImportController(IRegistrationService registrationService, IImportScoresService importScoresService, IMapper mapper)
        {
            _registrationService = registrationService;
            _importScoresService = importScoresService;
            _mapper = mapper;
        }

        [HttpPost("matchcompetitors")]
        public void MatchCompetitors([FromBody] ImportCompetitorsRequest request)
        {
            var competitors = JsonConvert.DeserializeObject<CompetitorImportModel[]>(request.FileContent);

            _registrationService.RegisterCompetitors(Guid.Parse(request.SelectedMatchId), competitors);
        }

        [HttpPost("ufpsmatchresults")]
        public void ImportUFPSScores([FromBody]ImportCompetitorsRequest request)
        {
            var scoreModels = request.FileContent.Split(Environment.NewLine)
                .Select(ParseCSVToImportCompetitorScoreApiModel);

            _importScoresService.ImportUFPSScoresForMatch(
                Guid.Parse(request.SelectedMatchId),
                _mapper.Map<ImportCompetitorScoreModel[]>(scoreModels));
        }

        private ImportCompetitorScoreApiModel ParseCSVToImportCompetitorScoreApiModel(string line)
        {
            var chunks = line.Split(',');
            return new ImportCompetitorScoreApiModel
                       {
                           CompetitorName = chunks[0],
                           StageName = chunks[1],
                           Class = int.Parse(chunks[2]),
                           CompetitorScore = new ScoreApiModel
                                                 {
                                                     Time = decimal.Parse(chunks[3]),
                                                     A = int.Parse(chunks[4]),
                                                     C = int.Parse(chunks[5]),
                                                     D = int.Parse(chunks[6]),
                                                     Miss = int.Parse(chunks[7]),
                                                     EngageError = int.Parse(chunks[8]),
                                                     PT = int.Parse(chunks[9]),
                                                     Proc = int.Parse(chunks[10]),
                                                     SpecialProc = int.Parse(chunks[11]),
                                                     UnpropperExecution = int.Parse(chunks[12])
                           }
                       };
        }
    }
}