﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using IPSCStatsOfficer.Services;
using IPSCStatsOfficer.WebSite.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace IPSCStatsOfficer.WebSite.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {
        private readonly ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpGet("login")]
        public LoginApiResponse Login(string userEmail)
        {
            var roles = _loginService.Login(userEmail);
            if (roles.Any())
            {
                var claims = roles.Select(x => new Claim(ClaimTypes.Role, x)).ToList();
                claims.Add(new Claim(ClaimTypes.Name, userEmail));
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var tokeOptions = new JwtSecurityToken(
                    "http://localhost:5000",
                    "http://localhost:5000",
                    claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: signinCredentials);

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return new LoginApiResponse { Token = tokenString, Roles = string.Join(',', roles) };
            }

            return new LoginApiResponse();
        }
    }
}