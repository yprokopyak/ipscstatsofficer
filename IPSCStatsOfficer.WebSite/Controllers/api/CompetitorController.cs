﻿using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Services;
using IPSCStatsOfficer.WebSite.Models;
using Microsoft.AspNetCore.Mvc;

namespace IPSCStatsOfficer.WebSite.Controllers.api
{
    [Route("api/[controller]")]
    public class CompetitorController
    {
        private readonly ICompetitorsService _competitorService;
        private readonly IMapper _mapper;

        public CompetitorController(ICompetitorsService competitorService, IMapper mapper)
        {
            _competitorService = competitorService;
            _mapper = mapper;
        }

        [HttpGet("getAll")]
        public CompetitorApiModel[] Get()
        {
            var competitors = _competitorService.GetAll();
            return _mapper.Map<CompetitorApiModel[]>(competitors)
                .OrderBy(x => x.LastName)
                .ThenBy(x => x.FirstName)
                .ToArray();
        }

        [HttpPost]
        public void Post([FromBody] CompetitorApiModel model)
        {
            _competitorService.CreateCompetitor(_mapper.Map<CompetitorModel>(model));
        }
    }
}