﻿using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Services;
using IPSCStatsOfficer.WebSite.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IPSCStatsOfficer.WebSite.Controllers.api
{
    [Route("api/[controller]")]
    public class MatchesController : Controller
    {
        private readonly IMatchService _matchService;
        private readonly IMapper _mapper;

        public MatchesController(IMatchService matchService, IMapper mapper)
        {
            _matchService = matchService;
            _mapper = mapper;
        }

        // GET: api/<controller>
        [HttpGet]
        public MatchApiModel[] Get()
        {
            var matches = _matchService.GetAll();
            return _mapper.Map<MatchApiModel[]>(matches);
        }

        [HttpPost("create"), Authorize(Roles = "superAdmin")]
        public void Post([FromBody]MatchApiModel match)
        {
            _matchService.Add(_mapper.Map<MatchModel>(match));
        }

        [HttpPost("addStage")]
        public string AddStage([FromBody]AddStagesRequest request)
        {
            var stageId = _matchService.AddStage(request.MatchId, _mapper.Map<StageModel>(request.Stage));
            return stageId.ToString();
        }
    }
}
