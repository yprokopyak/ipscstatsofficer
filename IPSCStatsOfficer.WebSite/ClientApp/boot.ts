import './scss/app.scss';
import 'bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faMarker, faSave, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Multiselect from 'vue-multiselect';
import VueCarousel from 'vue-carousel';
import Vue2Filters from 'vue2-filters';

library.add(faMarker, faSave, faTrashAlt);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('multiselect', Multiselect);

Vue.use(VueRouter);
Vue.use(VueCarousel);
Vue.use(Vue2Filters);

const routes = [
    { path: '/', component: require('./components/home/home.vue.html').default },
    { path: '/import', component: require('./components/import/import.vue.html').default },
    { path: '/match', component: require('./components/match/matchcreate.vue.html').default },
    { path: '/matchdesign', component: require('./components/match/matchdesign.vue.html').default },
    { path: '/competitor', component: require('./components/competitor/competitor.vue.html').default },
    { path: '/registration', component: require('./components/match/registration.vue.html').default },
    { path: '/registercompetitor', component: require('./components/match/registerCompetitor.vue.html').default },
    { path: '/scoresstage', component: require('./components/scores/submitstage.vue.html').default },
    { path: '/scorescompetitor', component: require('./components/scores/submitcompetitor.vue.html').default },
    { path: '/viewscoresmatch', component: require('./components/scores/viewmatch.vue.html').default },
    { path: '/verification', component: require('./components/scores/verification.vue.html').default }
];

export const eventBus = new Vue();

new Vue({
    el: '#app-root',
    router: new VueRouter({ mode: 'history', routes: routes }),
    render: h => h(require('./components/app/app.vue.html').default)
});

export default eventBus;