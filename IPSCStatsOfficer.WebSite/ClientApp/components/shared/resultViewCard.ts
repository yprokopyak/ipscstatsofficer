﻿import Vue from "vue";
import { ScoreByCriteria } from "../datastructures/score";
import { Component, Prop } from "vue-property-decorator";

@Component
export default class ResultViewCard extends Vue {
    @Prop() scores!: ScoreByCriteria[];
    @Prop() showDetailedScore!: boolean;
}