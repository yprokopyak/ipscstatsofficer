﻿import Vue from "vue";
import { Score } from "../datastructures/score";
import { Stage } from "../datastructures/UserClasses";
import { Component, Prop } from "vue-property-decorator";
import { Dataservices } from "../dataservices/dataservices";

@Component
export default class ScoreSheet extends Vue {
    @Prop() titleText!: string;
    @Prop() titleSubText!: string;
    @Prop() score!: Score;
    @Prop() stage!: Stage;
    @Prop() hasDQ!: boolean;

    isDQMode: boolean = false;
    enterPinMode: boolean = false;
    resultSentMode: boolean = false;
    isEnterResultsMode: boolean = false;

    dqReason: string = "";
    competitorPIN: string = "";

    mounted() {
        if (this.score.dq) {
            this.resultSentMode = true;
        } else {
            this.isEnterResultsMode = true;
        }
    }

    saveCompetitorScore(score: Score) {
        Dataservices.saveCompetitorScore(score, this.competitorPIN)
            .then(_ => {
                this.resetStates();
                this.resultSentMode = true;
                this.competitorPIN = "";
            });
    }

    dqCompetitor() {
        Dataservices.dqCompetitor(this.score.competitorId, this.score.matchId, this.dqReason)
            .then(_ => {
                this.hasDQ = true;
                this.resetStates();
                this.resultSentMode = true;
            });
    }

    hasAllScores() {
        return this.stage.shotCount === this.score.a + this.score.c + this.score.d + this.score.miss;
    }

    resetStates() {
        this.isDQMode = false;
        this.enterPinMode = false;
        this.resultSentMode = false;
        this.isEnterResultsMode = false;
    }
}