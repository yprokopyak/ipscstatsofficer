﻿import Vue from "vue";
import { Match } from "../datastructures/UserClasses";
import { Component, Watch } from "vue-property-decorator";
import { Dataservices } from '../dataservices/dataservices';

@Component
export default class MatchSelector extends Vue {
    matchesList: Match[] = [];
    selectedMatch: Match = new Match();

    mounted() {
        Dataservices.getAllMatches().then(result => {
            this.matchesList = result;
            this.selectedMatch = this.matchesList[0];
            this.$emit('selectedMatchChanged', this.selectedMatch);
        });
    }

    @Watch('selectedMatch') onMatchChanged(newValue: Match) {
        this.$emit('selectedMatchChanged', newValue);
    }
}