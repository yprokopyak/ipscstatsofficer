﻿import Vue from "vue";
import { Stage, Competitor } from "../datastructures/UserClasses";
import { Component, Prop } from "vue-property-decorator";
import { Dataservices } from "../dataservices/dataservices";

@Component
export default class StageEditor extends Vue {
    @Prop() matchId!: string;
    @Prop() stage!: Stage;
    @Prop() roList!: Competitor[];

    mounted() {
        var seniorRO = this.roList.filter(x => x.id === this.stage.seniorRO);
      if (seniorRO.length > 0)
            this.stage.seniorROObj = seniorRO[0];

        var juniorRO = this.roList.filter(x => x.id === this.stage.juniorRO);
        if (juniorRO.length > 0)
            this.stage.juniorROObj = juniorRO[0];

        this.$forceUpdate();
    }

    saveStage(stage: Stage) {
        this.stage.seniorRO = this.stage.seniorROObj.id;
        if (this.stage.juniorROObj !== null) {
            this.stage.juniorRO = this.stage.juniorROObj.id;
        } else {
            this.stage.juniorRO = '';
        }
        Dataservices.saveStage(this.matchId, stage)
            .then(id => {
                stage.id = id;
                stage.editMode = false;
            });
    }

    roDisplayText(ro: Competitor) {
        return ro.lastName + " " + ro.firstName;
    }

    isStageSaved() : boolean {
        return this.stage.id !== "";
    }
}