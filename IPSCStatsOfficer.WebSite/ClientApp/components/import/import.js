var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import Vue from 'vue';
import axios from 'axios';
import { Match } from '../datastructures/UserClasses';
import { Component } from 'vue-property-decorator';
var ImportComponent = /** @class */ (function (_super) {
    __extends(ImportComponent, _super);
    function ImportComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.fileData = "";
        _this.matchesList = [];
        _this.selectedMatch = new Match();
        return _this;
    }
    ImportComponent.prototype.mounted = function () {
        var _this = this;
        axios.get('api/matches')
            .then(function (responce) {
            _this.matchesList = responce.data;
            _this.selectedMatch = _this.matchesList[0];
        });
    };
    ImportComponent.prototype.importMatchCompetitors = function () {
        var _this = this;
        var fileDomElement = document.getElementById('importFile');
        if (fileDomElement != null && fileDomElement.files != null) {
            var file = fileDomElement.files[0];
            var reader = new FileReader();
            reader.onload = function (ev) {
                var fileReader = ev.target;
                if (fileReader != null && fileReader.result != null) {
                    var contents = fileReader.result.toString();
                    _this.uploadMatchCompetitors(contents);
                }
            };
            reader.readAsText(file);
        }
    };
    ImportComponent.prototype.uploadMatchCompetitors = function (fileContent) {
        var selectedMatchId = this.selectedMatch.id;
        axios.post('api/import/matchcompetitors', { fileContent: fileContent, selectedMatchId: selectedMatchId });
    };
    ImportComponent.prototype.addMatch = function () {
        var _this = this;
        axios.post('api/matches/add', {})
            .then(function (responce) {
            _this.selectedMatch = responce.data;
        });
    };
    ImportComponent = __decorate([
        Component
    ], ImportComponent);
    return ImportComponent;
}(Vue));
export default ImportComponent;
//# sourceMappingURL=import.js.map