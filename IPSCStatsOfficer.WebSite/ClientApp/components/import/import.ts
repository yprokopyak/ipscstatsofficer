﻿import Vue from 'vue';
import { Match } from '../datastructures/UserClasses';
import { Component } from 'vue-property-decorator';
import { Dataservices } from '../dataservices/dataservices';

@Component({
    components: {
        MatchSelector: require('../shared/matchSelector.vue.html').default
    }
})
export default class ImportComponent extends Vue {
    fileData: string = "";
    matchesList: Match[] = [];
    selectedMatch: Match = new Match();

    importMatchCompetitors() {
        var fileDomElement = <HTMLInputElement>document.getElementById('importFile');
        if (fileDomElement != null && fileDomElement.files != null) {
            var file = fileDomElement.files[0];
            const reader = new FileReader();
            reader.onload = ev => {
                var fileReader = (<FileReader>ev.target);
                if (fileReader != null && fileReader.result != null) {
                    var contents = fileReader.result.toString();
                    this.uploadMatchCompetitors(contents);
                }
            };
            reader.readAsText(file);
        }
    }

    importMatchResult() {
        var fileDomElement = <HTMLInputElement>document.getElementById('importFile');
        if (fileDomElement != null && fileDomElement.files != null) {
            var file = fileDomElement.files[0];
            const reader = new FileReader();
            reader.onload = ev => {
                var fileReader = (<FileReader>ev.target);
                if (fileReader != null && fileReader.result != null) {
                    var contents = fileReader.result.toString();
                    this.uploadMatchResult(contents);
                }
            };
            reader.readAsText(file);
        }
    }

    selectedMatchChanged(match: Match) {
        this.selectedMatch = match;
    }

    uploadMatchCompetitors(fileContent: string) {
        var selectedMatchId = this.selectedMatch.id;
        Dataservices.uploadMatchCompetitors(fileContent, selectedMatchId);
    }

    uploadMatchResult(fileContent: string) {
        var selectedMatchId = this.selectedMatch.id;
        Dataservices.uploadMatchResult(fileContent, selectedMatchId);
    }
}
