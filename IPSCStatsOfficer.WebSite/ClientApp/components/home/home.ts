﻿import Vue from 'vue';
import { Dataservices } from '../dataservices/dataservices';
import { Component } from 'vue-property-decorator';
import { eventBus } from '../../boot';

@Component({
    components: {
        CompetitorMenu: require('../navmenu/competitormenu.vue.html').default,
        MdMenu: require('../navmenu/mdmenu.vue.html').default,
        RoMenu: require('../navmenu/romenu.vue.html').default,
        GuestMenu: require('../navmenu/guestmenu.vue.html').default,
        SuperAdminMenu: require('../navmenu/superadminmenu.vue.html').default
    }
})
export default class HomeComponent extends Vue {
    userRole: string = 'notLoggedIn';
    userLogin: string = '';
    showMenu: boolean = true;
    currentPageNr: number = 0;

    mounted() {
        this.userRole = Dataservices.restoreAuth();
        eventBus.$emit('loggedIn', this.userRole);

        eventBus.$on('loggedOut',
            _ => {
                this.userRole = 'notLoggedIn';
            });
    }

    sendPwd() {
        //send pwd
        this.currentPageNr = 1;
    }

    login() {
        Dataservices.login(this.userLogin).then(roles => {
            this.userRole = roles;
            eventBus.$emit('loggedIn', this.userRole);
        });
    }
    logout() {
        this.userRole = 'notLoggedIn';
    }

    toggleMenu() {
        this.showMenu = !this.showMenu;
    }
}