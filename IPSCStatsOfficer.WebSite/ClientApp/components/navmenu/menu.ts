﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import { eventBus } from '../../boot';

@Component({
    components: {
        CompetitorMenu: require('../navmenu/competitormenu.vue.html').default,
        MdMenu: require('../navmenu/mdmenu.vue.html').default,
        RoMenu: require('../navmenu/romenu.vue.html').default,
        GuestMenu: require('../navmenu/guestmenu.vue.html').default,
        SuperAdminMenu: require('../navmenu/superadminmenu.vue.html').default
    }
})
export default class MenuComponent extends Vue {
    userRole: string = 'guest';
    userEmail: string = '';
    showMenu: boolean = true;

    mounted() {
        eventBus.$on('loggedIn', (roles) => {
            this.userRole = roles;
        });
    }

    logout() {
        this.userRole = 'guest';
        eventBus.$emit('loggedOut');
    }

    toggleMenu() {
        this.showMenu = !this.showMenu;
    }
}