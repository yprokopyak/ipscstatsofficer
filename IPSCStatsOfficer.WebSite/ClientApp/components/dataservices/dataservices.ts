﻿import axios from "axios";
import * as Toastr from "toastr";
import { Promise } from "es6-promise";

import { Competitor, Match, Stage, Registration, Verification } from "../datastructures/UserClasses";
import { Score, MatchScore } from "../datastructures/Score";

export class Dataservices {
    private static token: string = "";


    private static saveToken(token: string, roles: string) {
        localStorage.setItem("authData",
            JSON.stringify({
                token: token,
                roles: roles,
                validTill: (new Date()).setDate((new Date()).getDate() + 1)
            }));
    }

    static restoreAuth(): string {
        const authData = localStorage.getItem("authData");
        if (authData != null) {
            const parsed = JSON.parse(authData);
            if (parsed.validTill > new Date()) {
                this.token = parsed.token;
                return parsed.roles;
            }
        }
        return "notLoggedIn";
    }

    static login(userEmail: string): Promise<any> {
        return new Promise((resolve, reject) => {
            axios.get("api/login/login", { params: { userEmail: userEmail } })
                .then(responce => {
                    this.saveToken(responce.data.token, responce.data.roles);
                    this.token = responce.data.token;
                    resolve(responce.data.roles);
                });
        });
    }

    static createMatch(match: { name: string; date: string; weaponType: string; matchScoringType: string; CROId: string; MDId: string }): Promise<boolean> {
        return new Promise((resolve, reject) => {
                axios.post("api/matches/create",
                        match,
                        {
                            headers: { 'Authorization': `bearer ${this.token}` }
                        })
                    .then(_ => {
                        Toastr.success("Матч було створено");
                        resolve(true);
                    })
                    .catch(error => {
                        Toastr.error(error.response.data.error, "Помилка");
                    });
        });
    }

    static createCompetitor(competitor: Competitor): Promise<boolean> {
        return new Promise((resolve, reject) => {
                axios.post("api/competitor/",
                        competitor,
                        {
                            headers: { 'Authorization': `bearer ${this.token}` }
                        })
                    .then(_ => {
                        Toastr.success("Учасника було створено");
                        resolve(true);
                    })
                    .catch(error => {
                        Toastr.error(error.response.data.error, "Помилка");
                    });
        });
    }

    static uploadMatchCompetitors(fileContent: string, matchId: string) {
        axios.post("api/import/matchcompetitors",
                { fileContent, selectedMatchId: matchId },
                {
                    headers: { 'Authorization': `bearer ${this.token}` }
                })
            .then(_ => Toastr.success("Учасників було успішно імпортовано.", ""))
            .catch(error => {
                Toastr.error(error.response.data.error, "Помилка");
            });
    }

    static uploadMatchResult(fileContent: string, matchId: string) {
        axios.post("api/import/ufpsmatchresults",
                { fileContent, selectedMatchId: matchId },
                {
                    headers: { 'Authorization': `bearer ${this.token}` }
                })
            .then(_ => Toastr.success("Результати було успішно імпортовано.", ""))
            .catch(error => {
                Toastr.error(error.response.data.error, "Помилка");
            });
    }

    static getAllMatches(): Promise<Match[]> {
        return new Promise((resolve, reject) => {
            axios.get<Match[]>("api/matches",
                    {
                        headers: { 'Authorization': `bearer ${this.token}` }
                    })
                .then(responce => {
                    resolve(responce.data);
                }).catch(error => Toastr.error(error.response.data.error, "Помилка"));
        });
    }

    static getAllCompetitors(): Promise<Competitor[]> {
        return new Promise((resolve, reject) => {
            axios.get<Competitor[]>("api/competitor/getAll",
                    {
                        headers: { 'Authorization': `bearer ${this.token}` }
                    })
                .then(responce => {
                    resolve(responce.data);
                }).catch(error => Toastr.error(error.response.data.error, "Помилка"));
        });
    }

    static saveStage(matchId: string, stage: Stage): Promise<string> {
        return new Promise((resolve, reject) => {
            axios.post("api/matches/addStage",
                { matchId: matchId, stage: stage },
                {
                    headers: { 'Authorization': `bearer ${this.token}` }
                }).then(responce => {
                    resolve(responce.data);
                Toastr.success("Вправу було збережено.", "");
            }).catch(error => Toastr.error(error.response.data.error, "Помилка"));
        });
    }

    static getMatchRegistrations(matchId: string): Promise<Registration[]> {
        return new Promise((resolve, reject) => {
            axios.get<Registration[]>("api/registration/formatch",
                    {
                        headers: { 'Authorization': `bearer ${this.token}` },
                        params: {
                            matchId: matchId
                        }
                    })
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => Toastr.error(error.response.data.error, "Помилка"));
        });
    }
    static removeRegistration(matchId: string, competitorId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            axios.post("api/registration/delete",
                { matchId, competitorId },
                {
                    headers: { 'Authorization': `bearer ${this.token}` }
                }).then(_ => {
                Toastr.success("Учасника було видалено.", "");
                resolve();
            }).catch(error => Toastr.error(error.response.data.error, "Помилка"));
        });
    }

    static saveCompetitorScore(score: Score, pin: string): Promise<any> {
        return new Promise((resolve, reject) => {
        axios.post("api/scores/addcompetitorscore",
            { score, pin },
            {
                headers: { 'Authorization': `bearer ${this.token}` }
            }).then(_ => {
                Toastr.success("Результат було збережено.", "");
            resolve();
        }).catch(error => Toastr.error(error.response.data.error, "Помилка"));
        });
    }

    static dqCompetitor(competitorId: string, matchId: string, dqReason: string): Promise<any> {
        return new Promise((resolve, reject) => {
            axios.post("api/scores/dqCompetitor",
                    {
                        competitorId: competitorId,
                        matchId: matchId,
                        reason: dqReason
                },
                    {
                        headers: { 'Authorization': `bearer ${this.token}` }
                    })
                .then(_ => {
                    Toastr.error("Учасника було дискваліфіковано", "DQ");
                    resolve();
                })
                .catch(error => Toastr.error(error.response.data.error, "Помилка"));
        });
    }

    static getScoresForMatch(matchId: string): Promise<MatchScore> {
        return new Promise((resolve, reject) => {
            axios.get<MatchScore>("api/scores/scoresForMatch",
                    {
                        headers: { 'Authorization': `bearer ${this.token}` },
                        params: {
                            matchId: matchId
                        }
                    })
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => Toastr.error(error.response.data.error, "Помилка"));
        });
    }

    static getVerificatinoForMatch(matchId: string): Promise<Verification[]> {
        return new Promise((resolve, reject) => {
            axios.get<Verification[]>("api/scores/verificationForMatch",
                {
                    headers: { 'Authorization': `bearer ${this.token}` },
                    params: {
                    matchId: matchId
                }
                })
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => Toastr.error(error.response.data.error, "Помилка"));;
        });
    }

    static registerCompetitor(matchId: string,
        competitorId: string,
        clas: string,
        squadNr: number,
        category: string,
        powerFactor: string) {

        axios.post("api/registration/registerCompetitor",
            {
                matchId,
                competitorId,
                "class": clas,
                squadNr,
                category,
                powerFactor
            },
            {
                headers: { 'Authorization': `bearer ${this.token}` }
            }).then(_ => {
            Toastr.success("Учасника зареєстровано.", "");
        }).catch(error => Toastr.error(error.response.data.error, "Помилка"));
    }
}