﻿import Vue from "vue";
import { Match, Competitor, Registration } from "../datastructures/UserClasses";
import { Component } from "vue-property-decorator";
import { Dataservices } from '../dataservices/dataservices';
import { Enums } from "../datastructures/Enums";

@Component({
    components: {
        MatchSelector: require('../shared/matchSelector.vue.html').default
    }
})
export default class RegisterCompetitorComponent extends Vue {
    lists = new Enums();
    selectedMatch = new Match();
    competitors: Competitor[] = [];
    selectedCompetitor = new Competitor();
    category: string = "";
    powerFactor: string = "";
    clas: string = "";
    squadNr: number = 0;

    mounted() {
        Dataservices.getAllCompetitors().then(responce => {
            this.competitors = responce;
        });
    }

    registerCompetitor() {
        Dataservices.registerCompetitor(this.selectedMatch.id, this.selectedCompetitor.id,
            this.clas, this.squadNr, this.category, this.powerFactor);
    }

    competitorDisplayText(comp: Competitor) {
        return comp.lastName + ' ' + comp.firstName;
    }

    selectedMatchChanged(match: Match) {
        this.selectedMatch = match;
    }
}