﻿import Vue from "vue";
import { Match, Stage, Competitor } from "../datastructures/UserClasses";
import { Component, Watch } from "vue-property-decorator";
import { Dataservices } from '../dataservices/dataservices';

@Component({
    components: {
        MatchSelector: require('../shared/matchSelector.vue.html').default,
        StageEditor: require('../shared/stageEditor.vue.html').default
    }
})
export default class MatchDesignComponent extends Vue {
    selectedMatch = new Match();
    roList: Competitor[] = [];

    mounted() {
        Dataservices.getAllCompetitors().then(responce => {
            this.roList = responce;
        });
    }

    selectedMatchChanged(match: Match) {
        this.selectedMatch = match;
    }

    addStage() {
        this.selectedMatch.stages.push(new Stage());
    }
}