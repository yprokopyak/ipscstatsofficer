var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import Vue from 'vue';
import axios from 'axios';
import { Competitor } from '../datastructures/UserClasses';
import { Component } from 'vue-property-decorator';
var MatchCreateComponent = /** @class */ (function (_super) {
    __extends(MatchCreateComponent, _super);
    function MatchCreateComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.name = '';
        _this.date = '';
        _this.weapon = '';
        _this.roList = [];
        _this.cro = new Competitor();
        _this.md = new Competitor();
        return _this;
    }
    MatchCreateComponent.prototype.mounted = function () {
        var _this = this;
        axios.get('api/competitor/getAll')
            .then(function (responce) {
            _this.roList = responce.data;
        });
    };
    MatchCreateComponent.prototype.createMatch = function () {
        axios.post('api/matches/create', {
            name: this.name,
            date: this.date,
            weaponType: this.weapon,
            CROId: this.cro.id,
            MDId: this.md.id
        });
    };
    MatchCreateComponent = __decorate([
        Component
    ], MatchCreateComponent);
    return MatchCreateComponent;
}(Vue));
export default MatchCreateComponent;
//# sourceMappingURL=matchcreate.js.map