﻿import Vue from 'vue';
import { Competitor } from '../datastructures/UserClasses';
import { Component } from 'vue-property-decorator';
import { Dataservices } from '../dataservices/dataservices';

@Component
export default class MatchCreateComponent extends Vue {
    name: string = '';
    date: string = '';
    weapon: string = '';
    matchScoringType: string = '';
    roList: Competitor[] = [];
    cro: Competitor = new Competitor();
    md: Competitor = new Competitor();

    mounted() {
        Dataservices.getAllCompetitors().then(responce => {
                this.roList = responce;
            });
    }

    createMatch() {
        Dataservices.createMatch({
            name: this.name,
            date: this.date,
            weaponType: this.weapon,
            matchScoringType: this.matchScoringType,
            CROId: this.cro.id,
            MDId: this.md.id
        }).then(result => {
            if (result) {
                this.name = "";
                this.date = "";
                this.weapon = "";
                this.matchScoringType = "";
                this.cro.id = "";
                this.md.id = "";
            }
        });
    }

    roDisplayText(ro: Competitor) {
        return ro.lastName + " " + ro.firstName;
    }
}
