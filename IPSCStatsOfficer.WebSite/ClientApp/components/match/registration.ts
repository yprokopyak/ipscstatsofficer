﻿import Vue from "vue";
import { Match, Registration } from "../datastructures/UserClasses";
import { Component } from "vue-property-decorator";
import { Dataservices } from '../dataservices/dataservices';


@Component({
    components: {
        MatchSelector: require('../shared/matchSelector.vue.html').default
    }
})
export default class RegistrationComponent extends Vue {
    selectedMatch = new Match();
    registrations: Registration[] = [];

    showCompetitors() {
        Dataservices.getMatchRegistrations(this.selectedMatch.id).then(response => {
            this.registrations = response;
        });
    }

    removeRegistration(reg: Registration) {
        Dataservices.removeRegistration(this.selectedMatch.id, reg.id).then(_ => {
            this.registrations = this.registrations.filter(x => x.id !== reg.id);
        });
    }

    selectedMatchChanged(match: Match) {
        this.selectedMatch = match;
        this.registrations = [];
        this.showCompetitors();
    }
}