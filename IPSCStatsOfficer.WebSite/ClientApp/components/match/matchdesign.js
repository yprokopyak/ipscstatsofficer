var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import Vue from "vue";
import axios from "axios";
import { Match, Stage } from "../datastructures/UserClasses";
import { Component } from "vue-property-decorator";
var MatchDesignComponent = /** @class */ (function (_super) {
    __extends(MatchDesignComponent, _super);
    function MatchDesignComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.matchesList = [];
        _this.selectedMatch = new Match();
        _this.roList = [];
        return _this;
    }
    MatchDesignComponent.prototype.mounted = function () {
        var _this = this;
        axios.get("api/matches")
            .then(function (response) {
            _this.matchesList = response.data;
            _this.selectedMatch = _this.matchesList[0];
        });
        axios.get('api/competitor/getAll')
            .then(function (responce) {
            _this.roList = responce.data;
        });
    };
    MatchDesignComponent.prototype.addStage = function () {
        this.selectedMatch.stages.push(new Stage());
    };
    MatchDesignComponent.prototype.saveStages = function () {
        axios.post('api/matches/addStages', { matchId: this.selectedMatch.id, stages: this.selectedMatch.stages });
    };
    MatchDesignComponent = __decorate([
        Component
    ], MatchDesignComponent);
    return MatchDesignComponent;
}(Vue));
export default MatchDesignComponent;
//# sourceMappingURL=matchdesign.js.map