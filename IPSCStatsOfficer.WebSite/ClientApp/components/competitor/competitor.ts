﻿import Vue from 'vue';
import { Competitor } from '../datastructures/UserClasses';
import { Component } from 'vue-property-decorator';
import { Dataservices } from '../dataservices/dataservices';

@Component
export default class CompetitorComponent extends Vue {
    competitor: Competitor = new Competitor();

    createCompetitor() {
        Dataservices.createCompetitor(this.competitor)
            .then(result => {
                if (result) {
                    this.competitor = new Competitor();
                }
            });
    }
}
