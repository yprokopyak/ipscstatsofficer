﻿export class Score {
    matchId: string;
    competitorId: string;
    stageId: string;
    time: number;
    a: number;
    c: number;
    d: number;
    miss: number;
    pt: number;
    proc: number;
    dq: boolean;

    constructor() {
        this.matchId = "";
        this.competitorId = "";
        this.stageId = "";
        this.time = 0.00;
        this.a = 0;
        this.c = 0;
        this.d = 0;
        this.miss = 0;
        this.pt = 0;
        this.proc = 0;
        this.dq = false;
    }
}

export class VerificationResult {
    time: string;
    a: number;
    c: number;
    d: number;
    miss: number;
    pt: number;
    proc: number;
    engageError: number;
    specialProc: number;
    unpropperExecution: number;
    competitorName: string;
    hitFactor: string;
    stagePoints: string;
    percentage: string;

    constructor() {
        this.time = "0.00";
        this.a = 0;
        this.c = 0;
        this.d = 0;
        this.miss = 0;
        this.pt = 0;
        this.proc = 0;
        this.engageError = 0;
        this.specialProc = 0;
        this.unpropperExecution = 0;
        this.competitorName = "";
        this.hitFactor = "0.0000";
        this.stagePoints = "0.0000";
        this.percentage = "0.00";
    }
}

export class ScoreByCriteria {
    description: string;
    result: ScoreResult[];
    subCriteriaScore: ScoreByCriteria[];

    constructor() {
        this.description = '';
        this.result = [];
        this.subCriteriaScore = [];
    }
}

export class MatchScore {
    byCategory: ScoreByCriteria[];
    byClass: ScoreByCriteria[];
    byStage: ScoreByCriteria[];
    byStageAndCategory: ScoreByCriteria[];

    constructor() {
        this.byCategory = [];
        this.byClass = [];
        this.byStageAndCategory = [];
        this.byStage = [];
    }
}

export class ScoreResult {
    competitorCriteria: string = '';
    competitorName: string = '';
    hitFactor: string = '';
    percentage: string = '';
    points: string = '';
    score: VerificationResult = new VerificationResult();

    contructor() {
        this.competitorCriteria = '';
        this.competitorName = '';
        this.hitFactor = '';
        this.percentage = '';
        this.points = '';
    }
}