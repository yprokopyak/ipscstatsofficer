﻿export class Enums {
    classes: string[];
    categories: string[];
    powerFactors: string[];

    constructor() {
        this.classes = ["Стандарт-мануал", "Стандарт", "Модифікований", "Відкритий", "Пістолетний калібр", "Напівавтоматичний-відкритий SAO", "Напівавтоматичний стандартний SAS"];
        this.categories = ["Загальна", "Леді", "Юніор", "Сеньйор", "Суперсеньйор"];
        this.powerFactors = ["Мінор", "Мажор"];
    }
}