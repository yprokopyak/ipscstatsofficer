var Score = /** @class */ (function () {
    function Score() {
        this.matchId = "";
        this.competitorId = "";
        this.stageId = "";
        this.time = 0.00;
        this.a = 0;
        this.c = 0;
        this.d = 0;
        this.miss = 0;
        this.pt = 0;
        this.proc = 0;
        this.targets = [];
        this.penaltyTargets = [];
    }
    return Score;
}());
export { Score };
var ResultScore = /** @class */ (function () {
    function ResultScore() {
        this.time = "0.00";
        this.a = 0;
        this.c = 0;
        this.d = 0;
        this.miss = 0;
        this.pt = 0;
        this.proc = 0;
        this.competitorName = "";
        this.hitFactor = "0.0000";
        this.stagePoints = "0.0000";
        this.percentage = "0.00";
    }
    return ResultScore;
}());
export { ResultScore };
var StageScore = /** @class */ (function () {
    function StageScore() {
        this.classDescription = "";
        this.results = [];
    }
    return StageScore;
}());
export { StageScore };
//# sourceMappingURL=score.js.map