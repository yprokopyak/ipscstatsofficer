﻿import { Score } from "../datastructures/score";

export class Stage {
    id: string;
    name: string;
    shotCount: number;
    penaltyTargetsCount: number;
    ammoType: string;
    stageMaxPoints: number;
    seniorRO: string;
    juniorRO: string;
    seniorROObj: Competitor;
    juniorROObj: Competitor;
    editMode: boolean;

    constructor() {
        this.id = "";
        this.name = "";
        this.shotCount = 0;
        this.penaltyTargetsCount = 0;
        this.ammoType = "Куля";
        this.seniorRO = "";
        this.juniorRO = "";
        this.stageMaxPoints = 40;
        this.editMode = true;

        this.seniorROObj = new Competitor();
        this.juniorROObj = new Competitor();
    }

    isSaved(): boolean {
        return this.id !== "";
    }
}

export class Match {
    id: string;
    name: string;
    date: string;
    weaponType: string;
    CROId: string;
    MDId: string;
    stages: Stage[];

    constructor() {
        this.id = "";
        this.name = "";
        this.date = "";
        this.weaponType = "";
        this.CROId = "";
        this.MDId = "";
        this.stages = [];
    }
}

export class Competitor {
    id: string;
    firstName: string;
    lastName: string;
    phone: string;
    email: string;
    category: string;

    constructor() {
        this.id = "";
        this.firstName = "";
        this.lastName = "";
        this.phone = "";
        this.email = "";
        this.category = "";
    }
}

export class Registration {
    id: string;
    name: string;
    class: string;
    category: string;
    squad: number;
    hasDQ: boolean;
    dqReason: string;
    scores: Score[];

    constructor() {
        this.name = "";
        this.class = "";
        this.category = "";
        this.squad = 0;
        this.id = "";
        this.hasDQ = false;
        this.dqReason = "";
        this.scores = [];
    }
}

export class Verification {
    competitor: Competitor;
    scores: [Stage, Score][];

constructor() {
    this.competitor = new Competitor();
    this.scores = [];
}
}