var Stage = /** @class */ (function () {
    function Stage() {
        this.id = "";
        this.name = "";
        this.shotCount = 0;
        this.penaltyTargetsCount = 0;
        this.ammoType = "Куля";
        this.seniorRO = "";
        this.juniorRO = "";
        this.stageMaxPoints = 40;
    }
    return Stage;
}());
export { Stage };
var Match = /** @class */ (function () {
    function Match() {
        this.id = "";
        this.name = "";
        this.date = "";
        this.weaponType = "";
        this.CROId = "";
        this.MDId = "";
        this.stages = [];
    }
    return Match;
}());
export { Match };
var Competitor = /** @class */ (function () {
    function Competitor() {
        this.id = "";
        this.firstName = "";
        this.lastName = "";
        this.phone = "";
        this.email = "";
        this.category = "";
    }
    return Competitor;
}());
export { Competitor };
var Registration = /** @class */ (function () {
    function Registration() {
        this.name = "";
        this.class = "";
        this.category = "";
        this.squad = "";
        this.id = "";
    }
    return Registration;
}());
export { Registration };
//# sourceMappingURL=UserClasses.js.map