﻿import Vue from "vue";
import { Match } from "../datastructures/UserClasses";
import { MatchScore } from "../datastructures/score";
import { Component } from "vue-property-decorator";
import { Dataservices } from '../dataservices/dataservices';

@Component({
    components: {
        MatchSelector: require('../shared/matchSelector.vue.html').default,
        ResultViewCard: require('../shared/resultViewCard.vue.html').default,
    }
})
export default class ViewStageScoresComponent extends Vue {
    selectedMatch = new Match();
    results: MatchScore = new MatchScore();

    loadResultsForMatch() {
        Dataservices.getScoresForMatch(this.selectedMatch.id)
            .then(responce => {
                this.results = responce;
            });
    }

    selectedMatchChanged(match: Match) {
        this.selectedMatch = match;
        this.loadResultsForMatch();
    }
}