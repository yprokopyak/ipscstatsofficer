﻿import Vue from "vue";
import { Match, Verification } from "../datastructures/UserClasses";
import { Component } from "vue-property-decorator";
import { Dataservices } from '../dataservices/dataservices';

@Component({
    components: {
        MatchSelector: require('../shared/matchSelector.vue.html').default
    }
})
export default class ViewStageScoresComponent extends Vue {
    selectedMatch = new Match();
    verification: Verification[] = [];

    loadVerificationForMatch() {
        Dataservices.getVerificatinoForMatch(this.selectedMatch.id)
            .then(responce => {
                this.verification = responce;
            });
    }

    selectedMatchChanged(match: Match) {
        this.selectedMatch = match;
        this.loadVerificationForMatch();
    }
}