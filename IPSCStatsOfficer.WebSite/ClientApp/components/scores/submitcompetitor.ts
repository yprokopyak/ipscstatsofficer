﻿import Vue from "vue";
import { Match, Stage, Registration } from "../datastructures/UserClasses";
import { Score } from "../datastructures/score";
import { Dataservices } from '../dataservices/dataservices';
import { Component, Watch } from "vue-property-decorator";

@Component({
    components: {
        MatchSelector: require('../shared/matchSelector.vue.html').default,
        ScoreSheet: require('../shared/scoreSheet.vue.html').default
    }
})
export default class SubmitStageScoresComponent extends Vue {
    matchesList: Match[] = [];
    registrations: Registration[] = [];
    selectedMatch = new Match();
    selectedCompetitor = new Registration();
    competitorResults: Array<[Stage, Score]> = [];

    loadRegistrationsForMatch() {
        Dataservices.getMatchRegistrations(this.selectedMatch.id)
            .then(response => {
                this.registrations = response;
            });
    }

    loadCompetitorResults() {
        this.competitorResults = [];
        var thisClosure = this;

        this.selectedMatch.stages.forEach(function (stage: Stage) {
            var score = thisClosure.selectedCompetitor.scores.filter(s =>
                s.matchId === thisClosure.selectedMatch.id &&
                s.competitorId === thisClosure.selectedCompetitor.id &&
                s.stageId === stage.id)[0];

            if (score == null) {
                score = new Score();
                score.competitorId = thisClosure.selectedCompetitor.id;
                score.matchId = thisClosure.selectedMatch.id;
                score.stageId = stage.id;
                score.dq = thisClosure.selectedCompetitor.hasDQ;
            }

            thisClosure.competitorResults.push(<[Stage, Score]>[stage, score]);
        });
    }

    selectedMatchChanged(match: Match) {
        this.selectedMatch = match;
        this.loadRegistrationsForMatch();
    }

    @Watch('selectedCompetitor') onCompetitorChanged(oldValue: string, newValue: string) {
        this.loadCompetitorResults();
    }
}