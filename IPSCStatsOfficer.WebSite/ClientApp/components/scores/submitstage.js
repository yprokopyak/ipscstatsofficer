var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import Vue from "vue";
import axios from "axios";
import { Match, Stage, Registration } from "../datastructures/UserClasses";
import { Score } from "../datastructures/score";
import { Target } from "../datastructures/target";
import { Component, Watch } from "vue-property-decorator";
var SubmitStageScoresComponent = /** @class */ (function (_super) {
    __extends(SubmitStageScoresComponent, _super);
    function SubmitStageScoresComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.matchesList = [];
        _this.squads = [];
        _this.registrations = [];
        _this.selectedMatch = new Match();
        _this.selectedStage = new Stage();
        _this.selectedSquad = 0;
        _this.squadRegistrations = {};
        _this.previousCompetitor = [new Registration(), new Score()];
        _this.currentCompetitor = [new Registration(), new Score()];
        _this.nextCompetitor = [new Registration(), new Score()];
        return _this;
    }
    SubmitStageScoresComponent.prototype.mounted = function () {
        var _this = this;
        axios.get("api/matches")
            .then(function (response) {
            _this.matchesList = response.data;
            _this.selectedMatch = _this.matchesList[0];
            _this.loadRegistrationsForMatch();
        });
    };
    SubmitStageScoresComponent.prototype.loadRegistrationsForMatch = function () {
        var _this = this;
        axios.get("api/competitor/formatch", {
            params: {
                matchId: this.selectedMatch.id
            }
        })
            .then(function (response) {
            _this.registrations = response.data;
            _this.squads = [];
            var squadsClosure = _this.squads;
            response.data.forEach(function (registration) {
                if (squadsClosure.indexOf(registration.squad) === -1) {
                    squadsClosure.push(registration.squad);
                }
            });
            _this.squads = squadsClosure.sort();
        });
    };
    SubmitStageScoresComponent.prototype.loadFirstCompetitorInSquad = function () {
        this.previousCompetitor = this.squadRegistrations[this.selectedSquad][this.squadRegistrations[this.selectedSquad].length - 1];
        this.currentCompetitor = this.squadRegistrations[this.selectedSquad][0];
        this.nextCompetitor = this.squadRegistrations[this.selectedSquad][1];
    };
    SubmitStageScoresComponent.prototype.moveToNextCompetitor = function () {
        this.previousCompetitor = this.currentCompetitor;
        this.currentCompetitor = this.nextCompetitor;
        var idxOfNext = this.squadRegistrations[this.selectedSquad].indexOf(this.nextCompetitor) + 1;
        if (idxOfNext >= this.squadRegistrations[this.selectedSquad].length) {
            idxOfNext = 0;
        }
        this.nextCompetitor = this.squadRegistrations[this.selectedSquad][idxOfNext];
    };
    SubmitStageScoresComponent.prototype.moveCompetitorsToSquads = function () {
        var _this = this;
        this.squads.forEach(function (squadNr) {
            _this.squadRegistrations[squadNr] = _this.registrations.filter(function (r) { return r.squad === squadNr; })
                .map(function (registration) {
                var score = new Score();
                score.matchId = _this.selectedMatch.id;
                score.competitorId = registration.id;
                score.stageId = _this.selectedStage.id;
                var targetArray = new Array();
                for (var i = 0; i < _this.selectedStage.shotCount; ++i) {
                    targetArray.push(new Target());
                }
                score.targets = targetArray;
                var penaltyArray = new Array();
                for (var j = 0; j < _this.selectedStage.penaltyTargetsCount; ++j) {
                    penaltyArray.push(new Target());
                }
                score.penaltyTargets = penaltyArray;
                return [registration, score];
            });
        });
    };
    SubmitStageScoresComponent.prototype.moveToPrevCompetitor = function () {
        this.nextCompetitor = this.currentCompetitor;
        this.currentCompetitor = this.previousCompetitor;
        var idxOfPrev = this.squadRegistrations[this.selectedSquad].indexOf(this.previousCompetitor) - 1;
        if (idxOfPrev <= 0) {
            idxOfPrev = this.squadRegistrations[this.selectedSquad].length - 1;
        }
        this.previousCompetitor = this.squadRegistrations[this.selectedSquad][idxOfPrev];
    };
    SubmitStageScoresComponent.prototype.saveCompetitorScore = function () {
        console.log('saving');
        console.log(this.currentCompetitor[1]);
        axios.post('api/scores/addcompetitorscore', this.currentCompetitor[1]);
    };
    SubmitStageScoresComponent.prototype.onMatchChanged = function (oldValue, newValue) {
        this.loadRegistrationsForMatch();
    };
    SubmitStageScoresComponent.prototype.onSquadChanged = function (oldValue, newValue) {
        this.loadFirstCompetitorInSquad();
    };
    SubmitStageScoresComponent.prototype.onStageChanged = function (oldValue, newValue) {
        this.moveCompetitorsToSquads();
    };
    SubmitStageScoresComponent.prototype.saveScores = function () {
        axios.post('api/matches/addStages', { matchId: this.selectedMatch.id, stages: this.selectedMatch.stages });
    };
    __decorate([
        Watch('selectedMatch')
    ], SubmitStageScoresComponent.prototype, "onMatchChanged", null);
    __decorate([
        Watch('selectedSquad')
    ], SubmitStageScoresComponent.prototype, "onSquadChanged", null);
    __decorate([
        Watch('selectedStage')
    ], SubmitStageScoresComponent.prototype, "onStageChanged", null);
    SubmitStageScoresComponent = __decorate([
        Component
    ], SubmitStageScoresComponent);
    return SubmitStageScoresComponent;
}(Vue));
export default SubmitStageScoresComponent;
//# sourceMappingURL=submitstage.js.map