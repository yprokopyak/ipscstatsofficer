﻿import Vue from "vue";
import { Match, Stage, Registration } from "../datastructures/UserClasses";
import { Score } from "../datastructures/score";
import { Dataservices } from '../dataservices/dataservices';
import { Component, Watch } from "vue-property-decorator";

@Component({
    components: {
        MatchSelector: require('../shared/matchSelector.vue.html').default,
        ScoreSheet: require('../shared/scoreSheet.vue.html').default,
    }
})
export default class SubmitStageScoresComponent extends Vue {
    matchesList: Match[] = [];
    squads: number[] = [];
    registrations: Registration[] = [];
    selectedMatch = new Match();
    selectedStage = new Stage();
    selectedSquad: number = -1;
    selectedSquadRegistrations: Array<[Registration, Score]> = [];
    currentPageNr: number = 0;

    loadRegistrationsForMatch() {
        Dataservices.getMatchRegistrations(this.selectedMatch.id)
            .then(response => {
                this.registrations = response;
                this.squads = [];
                var squadsClosure = this.squads;
                response.forEach(registration => {
                    if (squadsClosure.indexOf(registration.squad) === -1) {
                        squadsClosure.push(registration.squad);
                    }
                });
                this.squads = squadsClosure.sort();
            });
    }

    moveCompetitorsToSquads() {
        var selectedSquadRegistrations = this.registrations.filter(r => r.squad === this.selectedSquad)
            .map(registration => {
                var score = registration.scores.filter(s =>
                    s.matchId === this.selectedMatch.id && 
                    s.competitorId === registration.id &&
                    s.stageId === this.selectedStage.id)[0];
                
                if (score == null) {
                    score = new Score();
                    score.matchId = this.selectedMatch.id;
                    score.competitorId = registration.id;
                    score.stageId = this.selectedStage.id;
                    score.dq = registration.hasDQ;
                }

                    return <[Registration, Score]>[registration, score];
                });
            this.selectedSquadRegistrations = selectedSquadRegistrations;
    }

    selectedMatchChanged(match: Match) {
        this.selectedMatch = match;
        this.squads = [];
        this.selectedSquadRegistrations = [];
        this.selectedSquad = -1;
        this.loadRegistrationsForMatch();
    }

    gotoCompetitors() {
        this.currentPageNr = 1;
    }

    gotoDetailsSelection() {
        this.currentPageNr = 0;
    }

    getShortClassText(longText: string): string {
        switch (longText) {
            case "Стандарт-мануал":
                return "STM";
            case "Стандарт":
                return "STD";
            case "Модифікований":
                return "MDF";
            case "Відкритий":
                return "OPN";
            case "Пістолетний калібр":
                return "PCC";
            case "Напівавтоматичний-відкритий SAO":
                return "SAO";
            case "Напівавтоматичний стандартний SAS":
                return "SAS";
        }

        return longText;
    }

    @Watch('selectedStage') onStageChanged(oldValue: string, newValue: string) {
        if (this.selectedSquad !== -1)
            this.moveCompetitorsToSquads();
    }
    @Watch('selectedSquad') onSquadChanged(oldValue: string, newValue: string) {
        if (this.selectedStage.name.length > 0)
            this.moveCompetitorsToSquads();
    }
}