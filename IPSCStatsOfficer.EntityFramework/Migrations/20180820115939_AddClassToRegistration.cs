﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    public partial class AddClassToRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Class",
                table: "MatchRegistrations",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Class",
                table: "MatchRegistrations");
        }
    }
}
