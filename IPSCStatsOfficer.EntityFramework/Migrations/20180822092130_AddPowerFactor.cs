﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    public partial class AddPowerFactor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxPoints",
                table: "Stages",
                nullable: false,
                defaultValue: 40);

            migrationBuilder.AddColumn<int>(
                name: "PowerFactor",
                table: "MatchRegistrations",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxPoints",
                table: "Stages");

            migrationBuilder.DropColumn(
                name: "PowerFactor",
                table: "MatchRegistrations");
        }
    }
}
