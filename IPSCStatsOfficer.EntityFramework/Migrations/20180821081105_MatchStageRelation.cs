﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    public partial class MatchStageRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "MatchId",
                table: "Stages",
                nullable: false);

            migrationBuilder.CreateIndex(
                name: "IX_Stages_MatchId",
                table: "Stages",
                column: "MatchId");

            migrationBuilder.AddForeignKey(
                name: "FK_Stages_Matches_MatchId",
                table: "Stages",
                column: "MatchId",
                principalTable: "Matches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stages_Matches_MatchId",
                table: "Stages");

            migrationBuilder.DropIndex(
                name: "IX_Stages_MatchId",
                table: "Stages");

            migrationBuilder.DropColumn(
                name: "MatchId",
                table: "Stages");
        }
    }
}
