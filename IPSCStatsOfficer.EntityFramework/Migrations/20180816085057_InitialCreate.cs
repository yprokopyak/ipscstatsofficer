﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Competitors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    Category = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Competitors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MatchRegistrations",
                columns: table => new
                {
                    MatchId = table.Column<Guid>(nullable: false),
                    CompetitorId = table.Column<Guid>(nullable: false),
                    SquadNr = table.Column<int>(nullable: false),
                    RegisteredAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchRegistrations", x => new { x.MatchId, x.CompetitorId });
                    table.UniqueConstraint("AK_MatchRegistrations_CompetitorId_MatchId", x => new { x.CompetitorId, x.MatchId });
                });

            migrationBuilder.CreateTable(
                name: "Matches",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    StartAt = table.Column<DateTime>(nullable: false),
                    WeaponType = table.Column<int>(nullable: false),
                    CROId = table.Column<Guid>(nullable: false),
                    MDId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Matches", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Matches_Competitors_CROId",
                        column: x => x.CROId,
                        principalTable: "Competitors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Matches_Competitors_MDId",
                        column: x => x.MDId,
                        principalTable: "Competitors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Stages",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ShotCount = table.Column<int>(nullable: false),
                    PenaltyTargetsCount = table.Column<int>(nullable: false),
                    AmmoType = table.Column<int>(nullable: false),
                    SeniorROId = table.Column<Guid>(nullable: false),
                    JuniorROId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Stages_Competitors_JuniorROId",
                        column: x => x.JuniorROId,
                        principalTable: "Competitors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Stages_Competitors_SeniorROId",
                        column: x => x.SeniorROId,
                        principalTable: "Competitors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Results",
                columns: table => new
                {
                    MatchId = table.Column<Guid>(nullable: false),
                    CompetitorId = table.Column<Guid>(nullable: false),
                    Percentage = table.Column<decimal>(nullable: false),
                    Score = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Results", x => new { x.MatchId, x.CompetitorId });
                    table.UniqueConstraint("AK_Results_CompetitorId_MatchId_Percentage", x => new { x.CompetitorId, x.MatchId, x.Percentage });
                    table.ForeignKey(
                        name: "FK_Results_Competitors_CompetitorId",
                        column: x => x.CompetitorId,
                        principalTable: "Competitors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Results_Matches_MatchId",
                        column: x => x.MatchId,
                        principalTable: "Matches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Scores",
                columns: table => new
                {
                    MatchId = table.Column<Guid>(nullable: false),
                    CompetitorId = table.Column<Guid>(nullable: false),
                    StageId = table.Column<Guid>(nullable: false),
                    Time = table.Column<decimal>(nullable: false),
                    A = table.Column<int>(nullable: false),
                    C = table.Column<int>(nullable: false),
                    D = table.Column<int>(nullable: false),
                    Miss = table.Column<int>(nullable: false),
                    PT = table.Column<int>(nullable: false),
                    Proc = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Scores", x => new { x.MatchId, x.CompetitorId, x.StageId });
                    table.UniqueConstraint("AK_Scores_CompetitorId_MatchId_StageId", x => new { x.CompetitorId, x.MatchId, x.StageId });
                    table.ForeignKey(
                        name: "FK_Scores_Competitors_CompetitorId",
                        column: x => x.CompetitorId,
                        principalTable: "Competitors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Scores_Matches_MatchId",
                        column: x => x.MatchId,
                        principalTable: "Matches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Scores_Stages_StageId",
                        column: x => x.StageId,
                        principalTable: "Stages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Matches_CROId",
                table: "Matches",
                column: "CROId");

            migrationBuilder.CreateIndex(
                name: "IX_Matches_MDId",
                table: "Matches",
                column: "MDId");

            migrationBuilder.CreateIndex(
                name: "IX_Scores_StageId",
                table: "Scores",
                column: "StageId");

            migrationBuilder.CreateIndex(
                name: "IX_Stages_JuniorROId",
                table: "Stages",
                column: "JuniorROId");

            migrationBuilder.CreateIndex(
                name: "IX_Stages_SeniorROId",
                table: "Stages",
                column: "SeniorROId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MatchRegistrations");

            migrationBuilder.DropTable(
                name: "Results");

            migrationBuilder.DropTable(
                name: "Scores");

            migrationBuilder.DropTable(
                name: "Matches");

            migrationBuilder.DropTable(
                name: "Stages");

            migrationBuilder.DropTable(
                name: "Competitors");
        }
    }
}
