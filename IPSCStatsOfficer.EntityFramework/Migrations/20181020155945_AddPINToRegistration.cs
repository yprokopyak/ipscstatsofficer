﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    public partial class AddPINToRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PIN",
                table: "MatchRegistrations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PIN",
                table: "MatchRegistrations");
        }
    }
}
