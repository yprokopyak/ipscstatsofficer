﻿// <auto-generated />
using System;
using IPSCStatsOfficer.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    [DbContext(typeof(IPSCStatsOfficerDataContext))]
    partial class IPSCStatsOfficerDataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.3-rtm-32065")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Competitor", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("Phone")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Competitors");
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Match", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CROId");

                    b.Property<Guid>("MDId");

                    b.Property<int>("MatchScoringType");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<DateTime>("StartAt");

                    b.Property<int>("WeaponType");

                    b.HasKey("Id");

                    b.HasIndex("CROId");

                    b.HasIndex("MDId");

                    b.ToTable("Matches");
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.MatchRegistration", b =>
                {
                    b.Property<Guid>("MatchId");

                    b.Property<Guid>("CompetitorId");

                    b.Property<int>("Category");

                    b.Property<int>("Class");

                    b.Property<string>("DQReason");

                    b.Property<bool>("HasDQ");

                    b.Property<string>("PIN");

                    b.Property<int>("PowerFactor");

                    b.Property<DateTime>("RegisteredAt");

                    b.Property<int>("SquadNr");

                    b.HasKey("MatchId", "CompetitorId");

                    b.HasAlternateKey("CompetitorId", "MatchId");

                    b.ToTable("MatchRegistrations");
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Result", b =>
                {
                    b.Property<Guid>("MatchId");

                    b.Property<Guid>("CompetitorId");

                    b.Property<decimal>("Percentage");

                    b.Property<decimal>("Score");

                    b.HasKey("MatchId", "CompetitorId");

                    b.HasAlternateKey("CompetitorId", "MatchId", "Percentage");

                    b.ToTable("Results");
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Score", b =>
                {
                    b.Property<Guid>("MatchId");

                    b.Property<Guid>("CompetitorId");

                    b.Property<Guid>("StageId");

                    b.Property<int>("A");

                    b.Property<int>("C");

                    b.Property<int>("D");

                    b.Property<int>("EngageError");

                    b.Property<int>("Miss");

                    b.Property<int>("PT");

                    b.Property<int>("Proc");

                    b.Property<int>("SpecialProc");

                    b.Property<decimal>("Time");

                    b.Property<int>("UnpropperExecution");

                    b.HasKey("MatchId", "CompetitorId", "StageId");

                    b.HasAlternateKey("CompetitorId", "MatchId", "StageId");

                    b.HasIndex("StageId");

                    b.ToTable("Scores");
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Stage", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AmmoType");

                    b.Property<Guid?>("JuniorROId");

                    b.Property<Guid>("MatchId");

                    b.Property<int>("MaxPoints");

                    b.Property<string>("Name");

                    b.Property<int>("PenaltyTargetsCount");

                    b.Property<Guid>("SeniorROId");

                    b.Property<int>("ShotCount");

                    b.HasKey("Id");

                    b.HasIndex("JuniorROId");

                    b.HasIndex("MatchId");

                    b.HasIndex("SeniorROId");

                    b.ToTable("Stages");
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Match", b =>
                {
                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Competitor", "CRO")
                        .WithMany()
                        .HasForeignKey("CROId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Competitor", "MD")
                        .WithMany()
                        .HasForeignKey("MDId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.MatchRegistration", b =>
                {
                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Competitor", "Competitor")
                        .WithMany()
                        .HasForeignKey("CompetitorId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Result", b =>
                {
                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Competitor", "Competitor")
                        .WithMany()
                        .HasForeignKey("CompetitorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Match", "Match")
                        .WithMany()
                        .HasForeignKey("MatchId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Score", b =>
                {
                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Competitor", "Competitor")
                        .WithMany()
                        .HasForeignKey("CompetitorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Match", "Match")
                        .WithMany()
                        .HasForeignKey("MatchId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Stage", "Stage")
                        .WithMany()
                        .HasForeignKey("StageId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("IPSCStatsOfficer.EntityFramework.Entities.Stage", b =>
                {
                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Competitor", "JuniorRO")
                        .WithMany()
                        .HasForeignKey("JuniorROId");

                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Match", "Match")
                        .WithMany("Stages")
                        .HasForeignKey("MatchId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("IPSCStatsOfficer.EntityFramework.Entities.Competitor", "SeniorRO")
                        .WithMany()
                        .HasForeignKey("SeniorROId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
