﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    public partial class AddDQSupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DQReason",
                table: "MatchRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasDQ",
                table: "MatchRegistrations",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DQReason",
                table: "MatchRegistrations");

            migrationBuilder.DropColumn(
                name: "HasDQ",
                table: "MatchRegistrations");
        }
    }
}
