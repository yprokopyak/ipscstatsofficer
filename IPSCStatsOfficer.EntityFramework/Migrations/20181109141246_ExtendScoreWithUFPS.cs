﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    public partial class ExtendScoreWithUFPS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EngageError",
                table: "Scores",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SpecialProc",
                table: "Scores",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UnpropperExecution",
                table: "Scores",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EngageError",
                table: "Scores");

            migrationBuilder.DropColumn(
                name: "SpecialProc",
                table: "Scores");

            migrationBuilder.DropColumn(
                name: "UnpropperExecution",
                table: "Scores");
        }
    }
}
