﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IPSCStatsOfficer.EntityFramework.Migrations
{
    public partial class MoveCategoryToRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Category",
                table: "Competitors");

            migrationBuilder.AddColumn<int>(
                name: "Category",
                table: "MatchRegistrations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchRegistrations_Competitors_CompetitorId",
                table: "MatchRegistrations",
                column: "CompetitorId",
                principalTable: "Competitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MatchRegistrations_Competitors_CompetitorId",
                table: "MatchRegistrations");

            migrationBuilder.DropColumn(
                name: "Category",
                table: "MatchRegistrations");

            migrationBuilder.AddColumn<int>(
                name: "Category",
                table: "Competitors",
                nullable: false,
                defaultValue: 0);
        }
    }
}
