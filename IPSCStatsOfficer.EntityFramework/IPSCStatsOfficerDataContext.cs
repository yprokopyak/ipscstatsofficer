﻿using IPSCStatsOfficer.EntityFramework.Entities;
using Microsoft.EntityFrameworkCore;

namespace IPSCStatsOfficer.EntityFramework
{
    public class IPSCStatsOfficerDataContext : DbContext, IIPSCStatsOfficerDataContext
    {
        public IPSCStatsOfficerDataContext()
        {
        }

        public IPSCStatsOfficerDataContext(DbContextOptions<IPSCStatsOfficerDataContext> options)
            :base(options)
        {
        }

        public DbSet<Competitor> Competitors { get; set; }

        public DbSet<Match> Matches { get; set; }

        public DbSet<MatchRegistration> MatchRegistrations { get; set; }

        public DbSet<Result> Results { get; set; }

        public DbSet<Score> Scores { get; set; }

        public DbSet<Stage> Stages { get; set; }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MatchRegistration>().HasKey(x => new {x.MatchId, x.CompetitorId});
            modelBuilder.Entity<MatchRegistration>().Ignore(e => e.Scores);
            modelBuilder.Entity<Result>().HasKey(x => new {x.MatchId, x.CompetitorId});
            modelBuilder.Entity<Score>().HasKey(x => new {x.MatchId, x.CompetitorId, x.StageId});

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            ///*DEV*/optionsBuilder.UseSqlServer("Data Source=localhost\\SQLEXPRESS;Initial Catalog=IPSCStatsOfficer;Integrated Security=SSPI;");
            /*PROD*/optionsBuilder.UseSqlServer("Data Source=SQL6006.site4now.net;Initial Catalog=DB_A41F31_yosypprokopyak;User Id=DB_A41F31_yosypprokopyak_admin;Password=FnruyJEiE9GbexL;");
            //optionsBuilder.UseSqlServer("Data Source=den1.mssql6.gear.host;Initial Catalog=IPSCStatsOfficer;User Id=ipscstatsofficer;Password=Kf3GA_c3iPd!");
        }
    }
}
