﻿using IPSCStatsOfficer.EntityFramework.Entities;
using Microsoft.EntityFrameworkCore;

namespace IPSCStatsOfficer.EntityFramework
{
    public interface IIPSCStatsOfficerDataContext
    {
        DbSet<Competitor> Competitors { get; set; }
        DbSet<Match> Matches { get; set; }
        DbSet<MatchRegistration> MatchRegistrations { get; set; }
        DbSet<Result> Results { get; set; }
        DbSet<Score> Scores { get; set; }
        DbSet<Stage> Stages { get; set; }

        void SaveChanges();
    }
}
