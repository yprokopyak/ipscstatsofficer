﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPSCStatsOfficer.EntityFramework.Entities
{
    public class MatchRegistration
    {
        [Key, Column(Order = 0)]
        public Guid MatchId { get; set; }
        [Key, Column(Order = 1)]
        public Guid CompetitorId { get; set; }
        [Required]
        public int Class { get; set; }
        [Required]
        public int SquadNr { get; set; }
        [Required]
        public int Category { get; set; }
        [Required]
        public int PowerFactor { get; set; }
        [Required]
        public DateTime RegisteredAt { get; set; }

        public string PIN { get; set; }

        public bool HasDQ { get; set; }
        public string DQReason { get; set; }

        public Competitor Competitor { get; set; }
        public Score[] Scores { get; set; }
    }
}
