﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IPSCStatsOfficer.EntityFramework.Entities
{
    public class Match
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime StartAt { get; set; }
        [Required]
        public int WeaponType { get; set; }

        public int MatchScoringType { get; set; }

        [Required]
        public Competitor CRO { get; set; }
        [Required]
        public Competitor MD { get; set; }

        public ICollection<Stage> Stages { get; set; }
    }
}
