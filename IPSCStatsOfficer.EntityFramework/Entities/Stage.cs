﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IPSCStatsOfficer.EntityFramework.Entities
{
    public class Stage
    {
        public Guid Id { get; set; }

        public Guid MatchId { get; set; }

        public string Name { get; set; }
        [Required]
        public int ShotCount { get; set; }
        public int PenaltyTargetsCount { get; set; }
        public int AmmoType { get; set; }
        [Required]
        public int MaxPoints { get; set; }

        [Required]
        public Guid SeniorROId { get; set; }
        public Guid? JuniorROId { get; set; }

        public Competitor SeniorRO { get; set; }
        public Competitor JuniorRO { get; set; }

        public Match Match { get; set; }
    }
}
