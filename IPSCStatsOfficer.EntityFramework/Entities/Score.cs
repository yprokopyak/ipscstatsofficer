﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPSCStatsOfficer.EntityFramework.Entities
{
    public class Score
    {
        [Key, Column(Order = 0)]
        public Guid MatchId { get; set; }
        [Key, Column(Order = 1)]
        public Guid CompetitorId { get; set; }
        [Key, Column(Order = 2)]
        public Guid StageId { get; set; }
        [Required]
        public decimal Time { get; set; }
        [Required]
        public int A { get; set; }
        [Required]
        public int C { get; set; }
        [Required]
        public int D { get; set; }
        [Required]
        public int Miss { get; set; }
        [Required]
        public int PT { get; set; }
        [Required]
        public int Proc { get; set; }
        [Required]
        public int EngageError { get; set; }
        [Required]
        public int SpecialProc { get; set; }
        [Required]
        public int UnpropperExecution { get; set; }

        public Match Match { get; set; }
        public Competitor Competitor { get; set; }
        public Stage Stage { get; set; }
    }
}
