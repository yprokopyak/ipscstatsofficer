﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IPSCStatsOfficer.EntityFramework.Entities
{
    public class Competitor
    {
        public Guid Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
    }
}
