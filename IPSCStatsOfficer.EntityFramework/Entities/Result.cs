﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IPSCStatsOfficer.EntityFramework.Entities
{
    public class Result
    {
        [Key, Column(Order = 0)]
        public Guid MatchId { get; set; }
        [Key, Column(Order = 1)]
        public Guid CompetitorId { get; set; }
        [Key, Column(Order = 2)]
        public decimal Percentage { get; set; }
        public decimal Score { get; set; }

        public Match Match { get; set; }
        public Competitor Competitor { get; set; }
}
}
