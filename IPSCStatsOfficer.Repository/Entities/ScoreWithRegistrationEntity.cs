﻿using IPSCStatsOfficer.EntityFramework.Entities;

namespace IPSCStatsOfficer.Repository.Entities
{
    public class ScoreWithRegistrationEntity
    {
        public Competitor Competitor { get; set; }

        public MatchRegistration Registration { get; set; }

        public Score Score { get; set; }

        public Stage Stage { get; set; }
    }
}