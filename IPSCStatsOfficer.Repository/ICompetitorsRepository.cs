﻿using System;
using IPSCStatsOfficer.Domain.DTO;

namespace IPSCStatsOfficer.Repository
{
    public interface ICompetitorsRepository
    {
        Guid Add(CompetitorDto competitor);

        CompetitorDto[] GetAll();

        CompetitorDto GetByEmail(string competitorEmail);

        Guid GetByNameOrAdd(string lastName, string firstName);
    }
}