﻿using System;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.EntityFramework;
using IPSCStatsOfficer.EntityFramework.Entities;

namespace IPSCStatsOfficer.Repository
{
    public class CompetitorsRepository: ICompetitorsRepository
    {
        private readonly IIPSCStatsOfficerDataContext _dbContext;
        private readonly IMapper _mapper;

        public CompetitorsRepository(IIPSCStatsOfficerDataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public CompetitorDto GetByEmail(string competitorEmail)
        {
            var competitor = _dbContext.Competitors.FirstOrDefault(x => x.Email == competitorEmail);
            return _mapper.Map<CompetitorDto>(competitor);
        }

        public Guid GetByNameOrAdd(string lastName, string firstName)
        {
            var competitor = _dbContext.Competitors.FirstOrDefault(x => x.FirstName == firstName
                                                                        && x.LastName == lastName);
            if (competitor == null)
            {
                competitor = new Competitor
                                 {
                                     Id = Guid.NewGuid(),
                                     LastName = lastName,
                                     FirstName = firstName,
                                     Email = "temp@temp.com",
                                     Phone = "temp"
                                 };
                _dbContext.Competitors.Add(competitor);
            }

            return competitor.Id;
        }

        public Guid Add(CompetitorDto competitor)
        {
            var entity = _mapper.Map<Competitor>(competitor);
            entity.Id = Guid.NewGuid();
            _dbContext.Competitors.Add(entity);
            return entity.Id;
        }

        public CompetitorDto[] GetAll()
        {
            var competitors = _dbContext.Competitors;
            return _mapper.Map<CompetitorDto[]>(competitors);
        }
    }
}
