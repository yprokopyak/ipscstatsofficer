﻿using IPSCStatsOfficer.EntityFramework;

namespace IPSCStatsOfficer.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IIPSCStatsOfficerDataContext _dbContext;

        public UnitOfWork(IIPSCStatsOfficerDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}
