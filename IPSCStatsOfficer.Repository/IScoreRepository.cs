﻿using System;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Models;

namespace IPSCStatsOfficer.Repository
{
    public interface IScoreRepository
    {
        void AddCompetitorScore(ScoreModel scoreModel);

        ScoreDto[] GetScoresForStage(Guid stageId);

        VerificationDto[] GetVerificationForMatch(Guid matchId);
    }
}