﻿using System;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.EntityFramework;
using IPSCStatsOfficer.EntityFramework.Entities;
using Microsoft.EntityFrameworkCore;

namespace IPSCStatsOfficer.Repository
{
    public class MatchRepository : IMatchRepository
    {
        private readonly IIPSCStatsOfficerDataContext _dbContext;
        private readonly IMapper _mapper;

        public MatchRepository(IIPSCStatsOfficerDataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public void Add(MatchDto matchDto)
        {
            var match = new Match
                            {
                                Id = Guid.NewGuid(),
                                Name = matchDto.Name,
                                StartAt = matchDto.StartAt,
                                WeaponType = matchDto.WeaponType,
                                MatchScoringType = matchDto.MatchScoringType,
                                MD = _dbContext.Competitors.FirstOrDefault(x => x.Id == matchDto.MDId),
                                CRO = _dbContext.Competitors.FirstOrDefault(x => x.Id == matchDto.CROId)
                            };

            _dbContext.Matches.Add(match);
        }

        public Guid AddStage(Guid matchId, StageDto stage)
        {
            var stageEntity = _mapper.Map<Stage>(stage);
            stageEntity.MatchId = matchId;

            if (stageEntity.Id == Guid.Empty)
            {
                stageEntity.Id = Guid.NewGuid();
                _dbContext.Stages.Add(stageEntity);
            }
            else
            {
                _dbContext.Stages.Update(stageEntity);

            }

            return stageEntity.Id;
        }

        public MatchDto[] All()
        {
            var matches = from match in _dbContext.Matches
                          join md in _dbContext.Competitors on match.MD equals md
                          join cro in _dbContext.Competitors on match.CRO equals cro
                          let stages =
                              from stage in _dbContext.Stages
                              join sro in _dbContext.Competitors on stage.SeniorRO equals sro
                              join jro in _dbContext.Competitors on stage.JuniorRO equals jro into jro
                              from junro in jro.DefaultIfEmpty()
                              where stage.MatchId == match.Id
                              select new StageDto
                                         {
                                             Id = stage.Id,
                                             Name = stage.Name,
                                             AmmoType = stage.AmmoType,
                                             MatchId = match.Id,
                                             PenaltyTargetsCount = stage.PenaltyTargetsCount,
                                             ShotCount = stage.ShotCount,
                                             MaxPoints = stage.MaxPoints,
                                             SeniorROId = sro.Id,
                                             JuniorROId = junro == null ? (Guid?)null : junro.Id
                                         }
                          select new MatchDto
                                     {
                                         CROId = cro.Id,
                                         Id = match.Id,
                                         MDId = md.Id,
                                         Name = match.Name,
                                         StartAt = match.StartAt,
                                         WeaponType = match.WeaponType,
                                         Stages = stages.ToArray()
                                     };

            return matches.ToArray();
        }

        public void DeleteStages(Guid matchId)
        {
            var stages = _dbContext.Stages.Where(x => x.MatchId == matchId);
            if (stages.Any())
                _dbContext.Stages.RemoveRange(stages);
        }

        public MatchDto GetById(Guid matchId)
        {
            var match = _dbContext.Matches.Include(x => x.MD)
                .Include(x => x.CRO)
                .Include(x => x.Stages)
                .FirstOrDefault(x => x.Id == matchId);

            return _mapper.Map<MatchDto>(match);
        }

        public void DisqualifyCompetitor(Guid competitorId, Guid matchId, string reason)
        {
            var registration = _dbContext.MatchRegistrations.FirstOrDefault(x=>x.MatchId == matchId && x.CompetitorId == competitorId);
            if (registration != null)
            { 
                registration.HasDQ = true;
                registration.DQReason = reason;
                _dbContext.MatchRegistrations.Update(registration);
            }
        }
    }
}