﻿using System;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.EntityFramework;
using IPSCStatsOfficer.EntityFramework.Entities;
using IPSCStatsOfficer.Repository.Entities;

namespace IPSCStatsOfficer.Repository
{
    public class ScoreRepository : IScoreRepository
    {
        private readonly IIPSCStatsOfficerDataContext _dbContext;
        private readonly IMapper _mapper;

        public ScoreRepository(IIPSCStatsOfficerDataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public void AddCompetitorScore(ScoreModel scoreModel)
        {
            var score = _mapper.Map<Score>(scoreModel);
            if (_dbContext.Scores.Any(
                x => x.StageId == score.StageId && x.MatchId == score.MatchId && x.CompetitorId == score.CompetitorId))
                _dbContext.Scores.Update(score);
            else
                _dbContext.Scores.Add(score);
        }

        public ScoreDto[] GetScoresForStage(Guid stageId)
        {
            var stageScores = from score in _dbContext.Scores
                              join competitor in _dbContext.Competitors on score.CompetitorId equals competitor.Id
                              join stage in _dbContext.Stages on score.StageId equals stage.Id
                              join registration in _dbContext.MatchRegistrations on
                                  new { m = score.MatchId, c = score.CompetitorId } equals
                                  new { m = registration.MatchId, c = registration.CompetitorId }
                              where score.StageId == stageId
                              select new ScoreWithRegistrationEntity
                                         {
                                             Score = score,
                                             Stage = stage,
                                             Competitor = competitor,
                                             Registration = registration
                                         };
            return _mapper.Map<ScoreDto[]>(stageScores);
        }

        public VerificationDto[] GetVerificationForMatch(Guid matchId)
        {
            var compScores = (from reg in _dbContext.MatchRegistrations
                              join comp in _dbContext.Competitors on reg.CompetitorId equals comp.Id
                              join stage in _dbContext.Stages on matchId equals stage.MatchId
                              join score in _dbContext.Scores on new { c = reg.CompetitorId, m = matchId, s = stage.Id } equals
                                  new { c = score.CompetitorId, m = score.MatchId, s = score.StageId } into scores
                              from score in scores.DefaultIfEmpty()
                              where reg.MatchId == matchId
                              select new { comp, score, stage }).ToList();

            return compScores.GroupBy(x => x.comp.Id)
                .Select(
                    x => new VerificationDto
                             {
                                 Competitor = _mapper.Map<CompetitorDto>(x.FirstOrDefault().comp),
                                 Scores = x.Select(
                                     y => (_mapper.Map<StageDto>(y.stage), _mapper.Map<ScoreDto>(y.score)))
                                 .OrderBy(s => s.Item1.Name)
                                 .ToArray()
                             })
                .ToArray();
        }
    }
}