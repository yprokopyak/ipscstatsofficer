﻿namespace IPSCStatsOfficer.Repository
{
    public interface IUnitOfWork
    {
        void SaveChanges();
    }
}
