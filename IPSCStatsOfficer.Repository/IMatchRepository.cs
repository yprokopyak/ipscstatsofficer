﻿using System;
using IPSCStatsOfficer.Domain.DTO;

namespace IPSCStatsOfficer.Repository
{
    public interface IMatchRepository
    {
        void Add(MatchDto matchDto);

        Guid AddStage(Guid matchId, StageDto stages);

        MatchDto[] All();

        void DeleteStages(Guid matchId);

        MatchDto GetById(Guid matchId);

        void DisqualifyCompetitor(Guid competitorId, Guid matchId, string reason);
    }
}