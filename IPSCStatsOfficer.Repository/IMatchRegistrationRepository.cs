﻿using System;
using IPSCStatsOfficer.Domain.DTO;

namespace IPSCStatsOfficer.Repository
{
    public interface IMatchRegistrationRepository
    {
        void Add(
            Guid matchId,
            Guid competitorId,
            int competitorClass,
            int competitorCategory,
            int competitorSquad,
            DateTime registeredAt);

        void Add(RegistrationDto registrationDto);

        RegistrationDto[] GetMatchRegistrations(Guid matchId);

        RegistrationDto GetRegistration(Guid matchId, Guid competitorId);

        void RemoveRegistration(Guid matchId, Guid competitorId);

        void UpdateRegistrationPin(Guid matchId, Guid competitorId, string pin);
    }
}