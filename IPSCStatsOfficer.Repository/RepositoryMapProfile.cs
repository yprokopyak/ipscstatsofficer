﻿using System;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Extentions;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Domain.Models.Scores;
using IPSCStatsOfficer.EntityFramework.Entities;
using IPSCStatsOfficer.Repository.Entities;

namespace IPSCStatsOfficer.Repository
{
    public class RepositoryProfile : Profile
    {
        public RepositoryProfile()
        {
            CreateMap<MatchDto, Match>();
            CreateMap<Match, MatchDto>()
                .ForMember(dst => dst.MDId, x => x.ResolveUsing(src => src.MD.Id))
                .ForMember(dst => dst.CROId, x => x.ResolveUsing(src => src.CRO.Id))
                .ForMember(dst => dst.Stages, x => x.ResolveUsing(src => src.Stages));

            CreateMap<MatchDto, MatchModel>()
                .ForMember(dst => dst.Stages, x => x.ResolveUsing(src => src.Stages));

            CreateMap<CompetitorDto, Competitor>();
            CreateMap<CompetitorDto, CompetitorModel>();
            CreateMap<CompetitorModel, CompetitorDto>();
            CreateMap<Competitor, CompetitorDto>();
            CreateMap<Score, ScoreDto>()
                .ForMember(dst => dst.CompetitorId, x => x.ResolveUsing(src => src.CompetitorId))
                .ForMember(dst => dst.StageId, x => x.ResolveUsing(src => src.StageId))
                .ForMember(dst => dst.MatchId, x => x.ResolveUsing(src => src.MatchId));

            CreateMap<ScoreDto, ScoreModel>();
            CreateMap<ScoreModel, Score>()
                .ForMember(dst => dst.Stage, x => x.Ignore());

            CreateMap<RegistrationDto, RegistrationModel>()
                .ForMember(dst => dst.Id, x => x.ResolveUsing(src => src.CompetitorId))
                .ForMember(dst => dst.Scores, x => x.ResolveUsing(src => src.Scores))
                .ForMember(dst => dst.Class, x => x.ResolveUsing(src => ((Classes)src.Class).GetEnumDescription()))
                .ForMember(
                    dst => dst.Category,
                    x => x.ResolveUsing(src => ((Categories)src.Category).GetEnumDescription()));

            CreateMap<MatchRegistration, RegistrationDto>()
                .ForMember(dst => dst.Squad, x => x.ResolveUsing(src => src.SquadNr))
                .ForMember(dst => dst.CompetitorId, x => x.ResolveUsing(src => src.CompetitorId))
                .ForMember(dst => dst.MatchId, x => x.ResolveUsing(src => src.MatchId))
                .ForMember(dst => dst.Competitor, x => x.ResolveUsing(src => src.Competitor))
                .ForMember(dst => dst.Category, x => x.ResolveUsing(src => src.Category))
                .ForMember(
                    dst => dst.Name,
                    x => x.ResolveUsing(src => src.Competitor != null
                                                   ? $"{src.Competitor.LastName} {src.Competitor.FirstName}"
                                                   : $"{nameof(src.Competitor)} is null."));

            CreateMap<RegistrationDto, MatchRegistration>()
                .ForMember(dst => dst.RegisteredAt, x => x.ResolveUsing(_ => DateTime.Now))
                .ForMember(dst => dst.SquadNr, x => x.ResolveUsing(src => src.Squad));

            CreateMap<Stage, StageDto>()
                .ForMember(dst => dst.Id, x => x.ResolveUsing(src => src.Id))
                .ForMember(dst => dst.SeniorROId, x => x.ResolveUsing(src => src.SeniorRO?.Id ?? Guid.Empty))
                .ForMember(dst => dst.JuniorROId, x => x.ResolveUsing(src => src.JuniorRO?.Id ?? Guid.Empty));

            CreateMap<StageDto, StageModel>()
                .ForMember(dst => dst.StageMaxPoints, x => x.ResolveUsing(src => src.MaxPoints));
            CreateMap<StageModel, StageDto>()
                .ForMember(dst => dst.MaxPoints, x => x.ResolveUsing(src => src.StageMaxPoints));
            CreateMap<StageDto, Stage>();

            CreateMap<VerificationModel, VerificationDto>();
            CreateMap<(StageDto, ScoreDto), (StageModel, ScoreModel)>()
                .ForMember(dst => dst.Item1, x => x.ResolveUsing(src => src.Item1))
                .ForMember(dst => dst.Item2, x => x.ResolveUsing(src => src.Item2));

            CreateMap<VerificationDto, VerificationModel>().ForMember(dst => dst.Scores, x => x.ResolveUsing(src => src.Scores));

            CreateMap<ScoreWithRegistrationEntity, ScoreDto>()
                .ForMember(dst => dst.A, x => x.ResolveUsing(src => src.Score.A))
                .ForMember(dst => dst.C, x => x.ResolveUsing(src => src.Score.C))
                .ForMember(dst => dst.D, x => x.ResolveUsing(src => src.Score.D))
                .ForMember(dst => dst.Miss, x => x.ResolveUsing(src => src.Score.Miss))
                .ForMember(dst => dst.PT, x => x.ResolveUsing(src => src.Score.PT))
                .ForMember(dst => dst.Proc, x => x.ResolveUsing(src => src.Score.Proc))
                .ForMember(dst => dst.Time, x => x.ResolveUsing(src => src.Score.Time))
                .ForMember(dst => dst.Stage, x => x.ResolveUsing(src => src.Stage))
                .ForMember(dst => dst.Competitor, x => x.ResolveUsing(src => src.Competitor))
                .ForMember(dst => dst.CompetitorRegistration, x => x.ResolveUsing(src => src.Registration));
        }
    }
}