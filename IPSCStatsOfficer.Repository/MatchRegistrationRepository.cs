﻿using System;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.EntityFramework;
using IPSCStatsOfficer.EntityFramework.Entities;
using Microsoft.EntityFrameworkCore;

namespace IPSCStatsOfficer.Repository
{
    public class MatchRegistrationRepository : IMatchRegistrationRepository
    {
        private readonly IIPSCStatsOfficerDataContext _dbContext;
        private readonly IMapper _mapper;

        public MatchRegistrationRepository(IIPSCStatsOfficerDataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public void Add(
            Guid matchId,
            Guid competitorId,
            int competitorClass,
            int competitorCategory,
            int competitorSquad,
            DateTime registeredAt)
        {
            _dbContext.MatchRegistrations.Add(
                new MatchRegistration
                    {
                        MatchId = matchId,
                        CompetitorId = competitorId,
                        SquadNr = competitorSquad,
                        Class = competitorClass,
                        Category = competitorCategory,
                        RegisteredAt = registeredAt
                    });
        }

        public void Add(RegistrationDto registrationDto)
        {
            _dbContext.MatchRegistrations.Add(_mapper.Map<MatchRegistration>(registrationDto));
        }

        public RegistrationDto[] GetMatchRegistrations(Guid matchId)
        {
            var registrations = _dbContext.MatchRegistrations.Include(x => x.Competitor)
                .Where(x => x.MatchId == matchId)
                .OrderBy(x => x.SquadNr)
                .ThenBy(x => x.Competitor.LastName)
                .ThenBy(x => x.Competitor.FirstName);

            foreach (var registration in registrations)
            {
                var scores = _dbContext.Scores.Where(
                    x => x.CompetitorId == registration.CompetitorId && x.MatchId == registration.MatchId);
                registration.Scores = scores.ToArray();
            }

            return _mapper.Map<RegistrationDto[]>(registrations);
        }

        public RegistrationDto GetRegistration(Guid matchId, Guid competitorId)
        {
            var registration =
                _dbContext.MatchRegistrations.FirstOrDefault(
                    x => x.MatchId == matchId && x.CompetitorId == competitorId);

            return _mapper.Map<RegistrationDto>(registration);
        }

        public void RemoveRegistration(Guid matchId, Guid competitorId)
        {
            var registration = _dbContext.MatchRegistrations.FirstOrDefault(x => x.MatchId == matchId && x.CompetitorId == competitorId);
            _dbContext.MatchRegistrations.Remove(registration ?? throw new InvalidOperationException("Реєстрацію не знайдено"));
        }

        public void UpdateRegistrationPin(Guid matchId, Guid competitorId, string pin)
        {
            var registration =
                _dbContext.MatchRegistrations.FirstOrDefault(
                    x => x.MatchId == matchId && x.CompetitorId == competitorId);

            if (registration != null)
                registration.PIN = pin;
        }
    }
}