﻿using System;
using System.Collections.Generic;
using System.Linq;
using IPSCStatsOfficer.Repository;

namespace IPSCStatsOfficer.Services
{
    public class LoginService : ILoginService
    {
        private readonly ICompetitorsRepository _competitorRepository;
        private readonly IMatchRepository _matchRepository;

        public LoginService(IMatchRepository matchRepository, ICompetitorsRepository competitorRepository)
        {
            _matchRepository = matchRepository;
            _competitorRepository = competitorRepository;
        }

        public string[] Login(string userEmail)
        {
            if (userEmail == "yprokopyak@gmail.com")
                return new[] { "superAdmin" };

            var competitor = _competitorRepository.GetByEmail(userEmail);
            if (competitor == null)
                throw new Exception("Користувача не знайдено");

            var matches = _matchRepository.All();
            var isMDorCRO = matches.Any(x => x.CROId == competitor.Id || x.MDId == competitor.Id);
            var isRO = matches.Any(
                x => x.Stages.Any(y => y.SeniorROId == competitor.Id || y.JuniorROId == competitor.Id));

            var list = new List<string>(2);
            if (isMDorCRO)
                list.Add("md");
            if (isRO)
                list.Add("ro");

            return list.ToArray();
        }
    }
}