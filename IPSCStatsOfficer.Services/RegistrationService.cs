﻿using System;
using System.Linq;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Extentions;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Repository;

namespace IPSCStatsOfficer.Services
{
    public class RegistrationService : IRegistrationService
    {
        private readonly ICompetitorsRepository _competitorRepository;
        private readonly IMapper _mapper;
        private readonly IMatchRegistrationRepository _matchRegistrationRepository;
        private readonly IMatchRepository _matchRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RegistrationService(
            IMatchRegistrationRepository matchRegistrationRepository,
            IMapper mapper,
            IUnitOfWork unitOfWork,
            IMatchRepository matchRepository,
            ICompetitorsRepository competitorRepository)
        {
            _matchRegistrationRepository = matchRegistrationRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _matchRepository = matchRepository;
            _competitorRepository = competitorRepository;
        }

        public RegistrationModel[] GetMatchRegistrations(Guid matchId)
        {
            return _mapper.Map<RegistrationModel[]>(_matchRegistrationRepository.GetMatchRegistrations(matchId));
        }

        public void RegisterCompetitor(RegistrationDto registrationDto)
        {
            _matchRegistrationRepository.Add(registrationDto);
            _unitOfWork.SaveChanges();
        }

        public void RegisterCompetitors(Guid matchId, CompetitorImportModel[] competitors)
        {
            var match = _matchRepository.GetById(matchId);
            if (match == null)
                throw new Exception("Матч не знайдено.");
            var uniqueCompetitors = competitors.GroupBy(x => x.Email).Select(x => x.First());

            foreach (var competitor in uniqueCompetitors)
            {
                var competitorDto = _competitorRepository.GetByEmail(competitor.Email);
                if (competitorDto == null)
                {
                    competitorDto = _mapper.Map<CompetitorDto>(competitor);
                    competitorDto.Id = _competitorRepository.Add(competitorDto);
                }

                _matchRegistrationRepository.Add(
                    match.Id,
                    competitorDto.Id,
                    (int)competitor.Class.GetValueFromDescription<Classes>(),
                    (int)competitor.Category.GetValueFromDescription<Categories>(),
                    competitor.Squad,
                    DateTime.Now);
            }

            _unitOfWork.SaveChanges();
        }

        public void RemoveRegistration(Guid matchId, Guid competitorId)
        {
            _matchRegistrationRepository.RemoveRegistration(matchId, competitorId);
            _unitOfWork.SaveChanges();
        }
    }
}