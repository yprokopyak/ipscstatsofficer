﻿using System;
using System.Linq;
using Autofac;
using AutoMapper;
using IPSCStatsOfficer.Domain.BusinessLogic.ScoresCalсulators;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Domain.Models.Scores;
using IPSCStatsOfficer.Repository;

namespace IPSCStatsOfficer.Services
{
    public class ScoreService : IScoreService
    {
        private readonly IMapper _mapper;
        private readonly IMatchRepository _matchRepository;
        private readonly IScoreRepository _scoreRepository;
        private readonly IMatchRegistrationRepository _matchRegistrationRepository;
        private readonly IUnitOfWork _unitOfWork;
        private IComponentContext _autofacContext;

        public ScoreService(
            IScoreRepository scoreRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IMatchRepository matchRepository,
            IMatchRegistrationRepository matchRegistrationRepository,
            IComponentContext autofaContext)
        {
            _matchRegistrationRepository = matchRegistrationRepository;
            _scoreRepository = scoreRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _matchRepository = matchRepository;
            _autofacContext = autofaContext;
        }

        public void AddCompetitorScore(ScoreModel scoreModel, string pin)
        {
            if (string.IsNullOrWhiteSpace(pin))
                throw new ArgumentException("PIN код не може бути порожнім.");

            var registration = _matchRegistrationRepository.GetRegistration(scoreModel.MatchId, scoreModel.CompetitorId);

            if (string.IsNullOrEmpty(registration.PIN))
            {
                _matchRegistrationRepository.UpdateRegistrationPin(scoreModel.MatchId, scoreModel.CompetitorId, pin);
                registration.PIN = pin;
            }

            if (registration.PIN != pin)
            {
                throw new ArgumentException("PIN код не співпадає.");
            }

            _scoreRepository.AddCompetitorScore(scoreModel);
            _unitOfWork.SaveChanges();
        }

        public MatchScoreModel ScoresForMatch(Guid matchId)
        {
            var scoreModel = new MatchScoreModel();
            var match = _matchRepository.GetById(matchId);
            var stages = match.Stages;
            var scoreCalculator = _autofacContext.ResolveKeyed<IScoresCalculator>(match.MatchScoringType);

            var scoresForStages = stages.ToDictionary(
                x => x.Id,
                x => _mapper.Map<ScoreDto[]>(_scoreRepository.GetScoresForStage(x.Id)));

            scoreModel.ByStage = stages
                .Select(x => scoreCalculator.CalculateScoresByStage(x.Name, scoresForStages[x.Id]))
                .OrderBy(x => x.CriteriaDescription)
                .ToArray();

            scoreModel.ByStageAndCategory = stages.Select(
                    x => scoreCalculator.CalculateScoresByStageAndCategory(x.Name, scoresForStages[x.Id]))
                .ToArray();

            scoreModel.ByClass = scoreCalculator.CalculateScoresByClass(
                scoreModel.ByStage,
                scoresForStages.SelectMany(x => x.Value).Select(x => x.CompetitorRegistration));

            scoreModel.ByCategory = scoreCalculator.CalculateScoresByClassAndCategory(
                scoreModel.ByStageAndCategory,
                scoresForStages.SelectMany(x => x.Value).Select(x => x.CompetitorRegistration));

            return scoreModel;
        }

        public VerificationModel[] VerificationForMatch(Guid matchId)
        {
            var verification = _scoreRepository.GetVerificationForMatch(matchId);
            return _mapper.Map<VerificationModel[]>(verification);
        }

        public void DisqualifyCompetitor(Guid competitorId, Guid matchId, string reason)
        {
            _matchRepository.DisqualifyCompetitor(competitorId, matchId, reason);
            _unitOfWork.SaveChanges();
        }
    }
}