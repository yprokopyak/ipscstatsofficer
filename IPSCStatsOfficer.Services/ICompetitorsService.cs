﻿using System;
using IPSCStatsOfficer.Domain.Models;

namespace IPSCStatsOfficer.Services
{
    public interface ICompetitorsService
    {
        void CreateCompetitor(CompetitorModel competitorModel);

        CompetitorModel[] GetAll();
    }
}