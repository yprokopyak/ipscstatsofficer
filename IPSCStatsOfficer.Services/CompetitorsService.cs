﻿using System;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Repository;

namespace IPSCStatsOfficer.Services
{
    public class CompetitorsService: ICompetitorsService
    {
        private readonly ICompetitorsRepository _competitorRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CompetitorsService(ICompetitorsRepository competitorRepository,
            IMapper mapper, IUnitOfWork unitOfWork)
        {
            _competitorRepository = competitorRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public void CreateCompetitor(CompetitorModel competitorModel)
        {
            if (competitorModel.Id == Guid.Empty)
            {
                competitorModel.Id = Guid.NewGuid();
            }

            _competitorRepository.Add(_mapper.Map<CompetitorDto>(competitorModel));
            _unitOfWork.SaveChanges();
        }

        public CompetitorModel[] GetAll()
        {
            var competitors = _competitorRepository.GetAll();
            return _mapper.Map<CompetitorModel[]>(competitors);
        }
    }
}
