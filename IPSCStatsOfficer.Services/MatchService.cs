﻿using System;
using AutoMapper;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Repository;

namespace IPSCStatsOfficer.Services
{
    public class MatchService : IMatchService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public MatchService(IMatchRepository matchRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _matchRepository = matchRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public void Add(MatchModel matchModel)
        {
            _matchRepository.Add(_mapper.Map<MatchDto>(matchModel));
            _unitOfWork.SaveChanges();
        }

        public MatchModel[] GetAll()
        {
            var matches = _matchRepository.All();
            return _mapper.Map<MatchModel[]>(matches);
        }

        public Guid AddStage(Guid matchId, StageModel stage)
        {
            var guid = _matchRepository.AddStage(matchId, _mapper.Map<StageDto>(stage));
            _unitOfWork.SaveChanges();
            return guid;
        }
    }
}
