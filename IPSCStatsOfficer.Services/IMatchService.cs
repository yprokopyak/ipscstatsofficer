﻿using System;
using IPSCStatsOfficer.Domain.Models;

namespace IPSCStatsOfficer.Services
{
    public interface IMatchService
    {
        void Add(MatchModel map);
        MatchModel[] GetAll();
        Guid AddStage(Guid matchId, StageModel stage);
    }
}
