﻿using System;
using System.Collections.Generic;
using System.Text;
using IPSCStatsOfficer.Domain.Models;

namespace IPSCStatsOfficer.Services
{
    public interface IImportScoresService
    {
        void ImportUFPSScoresForMatch(Guid matchId, ImportCompetitorScoreModel[] scores);
    }
}
