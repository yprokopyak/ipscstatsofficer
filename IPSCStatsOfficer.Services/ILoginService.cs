﻿using System.Collections.Generic;
using System.Text;

namespace IPSCStatsOfficer.Services
{
    public interface ILoginService
    {
        string[] Login(string userEmail);
    }
}
