﻿using System;
using IPSCStatsOfficer.Domain.DTO;
using IPSCStatsOfficer.Domain.Models;

namespace IPSCStatsOfficer.Services
{
    public interface IRegistrationService
    {
        RegistrationModel[] GetMatchRegistrations(Guid matchId);

        void RegisterCompetitor(RegistrationDto registrationDto);

        void RegisterCompetitors(Guid matchId, CompetitorImportModel[] competitors);

        void RemoveRegistration(Guid matchId, Guid competitorId);
    }
}