﻿using System;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Domain.Models.Scores;

namespace IPSCStatsOfficer.Services
{
    public interface IScoreService
    {
        void AddCompetitorScore(ScoreModel scoreModel, string pin);
        MatchScoreModel ScoresForMatch(Guid matchId);
        VerificationModel[] VerificationForMatch(Guid matchId);
        void DisqualifyCompetitor(Guid competitorId, Guid matchId, string reason);
    }
}