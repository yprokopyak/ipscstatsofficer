﻿using System;
using System.Collections.Generic;
using System.Linq;
using IPSCStatsOfficer.Domain.Models;
using IPSCStatsOfficer.Repository;

namespace IPSCStatsOfficer.Services
{
    public class ImportScoresService : IImportScoresService
    {
        private readonly IMatchRegistrationRepository _matchRegistrationRepository;
        private readonly IMatchRepository _matchRepository;
        private readonly ICompetitorsRepository _competitorsRepository;
        private readonly IScoreRepository _scoreRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ImportScoresService(
            IMatchRepository matchRepository,
            IScoreRepository scoreRepository,
            IMatchRegistrationRepository matchRegistrationRepository,
            IUnitOfWork unitOfWork,
            ICompetitorsRepository competitorsRepository)
        {
            _matchRepository = matchRepository;
            _scoreRepository = scoreRepository;
            _matchRegistrationRepository = matchRegistrationRepository;
            _unitOfWork = unitOfWork;
            _competitorsRepository = competitorsRepository;
        }

        public void ImportUFPSScoresForMatch(Guid matchId, ImportCompetitorScoreModel[] scores)
        {
            var flNameIdMapping = new Dictionary<string, Guid>();

            //1. Import Competitors
            foreach (var competitorScoreModel in scores)
            {
                if (!flNameIdMapping.ContainsKey(competitorScoreModel.CompetitorName))
                {
                    var competitorId = _competitorsRepository.GetByNameOrAdd(
                        competitorScoreModel.CompetitorName.Split(' ')[0].Trim(),
                        competitorScoreModel.CompetitorName.Split(' ')[1].Trim()
                        );
                    flNameIdMapping.Add(competitorScoreModel.CompetitorName, competitorId);
                }
            }
            _unitOfWork.SaveChanges();
            //2. Import Registrations
            foreach (var comp in flNameIdMapping)
            {
                _matchRegistrationRepository.Add(
                    matchId,
                    comp.Value,
                    scores.FirstOrDefault(x => x.CompetitorName == comp.Key)?.Class ?? 1,
                    (int)Categories.Regular,
                    0,
                    DateTime.Now);
            }
            _unitOfWork.SaveChanges();
            //3. Get stages
            var stages = _matchRepository.GetById(matchId).Stages.ToDictionary(x => x.Name, x => x.Id);

            //3. Import Score
            foreach (var score in scores)
            {
                _scoreRepository.AddCompetitorScore(new ScoreModel
                                                        {
                                                            MatchId = matchId,
                                                            StageId = stages[score.StageName],
                                                            CompetitorId = flNameIdMapping[score.CompetitorName],
                                                            Time = score.CompetitorScore.Time,
                                                            A = score.CompetitorScore.A,
                                                            C = score.CompetitorScore.C,
                                                            D = score.CompetitorScore.D,
                                                            Miss = score.CompetitorScore.Miss,
                                                            EngageError = score.CompetitorScore.EngageError,
                                                            PT = score.CompetitorScore.PT,
                                                            Proc = score.CompetitorScore.Proc,
                                                            SpecialProc = score.CompetitorScore.SpecialProc,
                                                            UnpropperExecution = score.CompetitorScore.UnpropperExecution
                                                        });
            }

            _unitOfWork.SaveChanges();
        }
    }
}